
<!-- Footer Full Width -->

<div class="footer footer-full-width" id="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">

				
				<!-- Widget About -->
				
				<div class="widget w-about">
				
<a href="02-ProfilePage.html" class="logo">
						<div class="img-wrap">
							<img src="img/logo-colored.png" alt="Olympus">
						</div>
						<div class="title-block">
							<h6 class="logo-title">Üniversitem.Online</h6>
							<div class="sub-title">Açık Kaynak Bilgi Platformu</div>
						</div>
					</a>
					<p>Türkiyenin açık kaynak sosyal bilgi ağı.</p>
					<ul class="socials">
						<li>
							<a href="https://www.facebook.com/universitemonline">
								<i class="fa fa-facebook-square" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-twitter" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-youtube-play" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-google-plus" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-instagram" aria-hidden="true"></i>
							</a>
						</li>
					</ul>
				</div>
				
				<!-- ... end Widget About -->

			</div>

			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">

				
				<!-- Widget List -->
				
				<div class="widget w-list">
					<h6 class="title">Main Links</h6>
					<ul>
						<li>
							<a href="#">Landing</a>
						</li>
						<li>
							<a href="#">Home</a>
						</li>
						<li>
							<a href="#">About</a>
						</li>
						<li>
							<a href="#">Events</a>
						</li>
					</ul>
				</div>
				
				<!-- ... end Widget List -->

			</div>
			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">

				
				<div class="widget w-list">
					<h6 class="title">Topluluklar</h6>
					<ul>
						<li>
							<a href="#">Üniversiteler</a>
						</li>
						<li>
							<a href="#">Şirketler</a>
						</li>
						<li>
							<a href="#">Eğitim Kurumları</a>
						</li>
						<li>
							<a href="#">Kaynak Siteler</a>
						</li>
					</ul>
				</div>

			</div>
			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">

				
				<div class="widget w-list">
					<h6 class="title">Özellikler</h6>
					<ul>
						<li>
							<a href="#">Makaleler</a>
						</li>
						<li>
							<a href="#">Duyurular</a>
						</li>
						<li>
							<a href="#">Etkinlikler</a>
						</li>
						<li>
							<a href="#">İş İlanları</a>
						</li>
					</ul>
				</div>

			</div>
			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">

				
				<div class="widget w-list">
					<h6 class="title">Üniversitem.Online</h6>
					<ul>
						<li>
							<a href="#">Gizlilik Sözleşmesi</a>
						</li>
						<li>
							<a href="#">Kullanım Koşulları</a>
						</li>
						<li>
							<a href="#">Forum</a>
						</li>
						<li>
							<a href="#">İstatistikler</a>
						</li>
					</ul>
				</div>

			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				
				<!-- SUB Footer -->
				
				<div class="sub-footer-copyright">
					<span>
						Copyright <a href="index.php">Üniversitem.Online</a> Tüm Hakları Saklıdır 2018
					</span>
				</div>
				
				<!-- ... end SUB Footer -->

			</div>
		</div>
	</div>
</div>

<!-- ... end Footer Full Width -->





<a class="back-to-top" href="#">
	<img src="svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>



<!-- JS Scripts -->
<script src="js/jquery-3.2.1.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script src="js/perfect-scrollbar.js"></script>
<script src="js/jquery.matchHeight.js"></script>
<script src="js/svgxuse.js"></script>
<script src="js/imagesloaded.pkgd.js"></script>
<script src="js/Headroom.js"></script>
<script src="js/velocity.js"></script>
<script src="js/ScrollMagic.js"></script>
<script src="js/jquery.waypoints.js"></script>
<script src="js/jquery.countTo.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/material.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/smooth-scroll.js"></script>
<script src="js/selectize.js"></script>
<script src="js/swiper.jquery.js"></script>
<script src="js/moment.js"></script>
<script src="js/daterangepicker.js"></script>
<script src="js/simplecalendar.js"></script>
<script src="js/fullcalendar.js"></script>
<script src="js/isotope.pkgd.js"></script>
<script src="js/ajax-pagination.js"></script>
<script src="js/Chart.js"></script>
<script src="js/chartjs-plugin-deferred.js"></script>
<script src="js/circle-progress.js"></script>
<script src="js/loader.js"></script>
<script src="js/run-chart.js"></script>
<script src="js/jquery.magnific-popup.js"></script>
<script src="js/jquery.gifplayer.js"></script>
<script src="js/mediaelement-and-player.js"></script>
<script src="js/mediaelement-playlist-plugin.min.js"></script>

<script src="js/base-init.js"></script>

<script src="Bootstrap/dist/js/bootstrap.bundle.js"></script>

</body>
</html>