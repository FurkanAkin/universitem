<?php
	class User {
		private $dbHost     = "localhost";
		private $dbUsername = "root";
		private $dbPassword = "";
		private $dbName     = "universitem";
		private $userTbl    = 'users';
		
		function __construct(){
			if(!isset($this->db)){
				// Connect to the database
				$this->db = new PDO("mysql:host=".$this->dbHost.";dbname=".$this->dbName.";charset=utf8",$this->dbUsername,$this->dbPassword);
				/*$conn = new mysqli($this->dbHost, $this->dbUsername, $this->dbPassword, $this->dbName);
				if($conn->connect_error){
					die("Failed to connect with MySQL: " . $conn->connect_error);
				}else{
					$this->db = $conn;
				}*/
			}
		}
		
		function checkUser($userData = array()){
			if(!empty($userData)){
				//Check whether user data already exists in database
				$prevQuery = "SELECT * FROM ".$this->userTbl." WHERE users_oauth_provider = '".$userData['oauth_provider']."' AND users_oauth_uid = '".$userData['oauth_uid']."'";
				$prevResult = $this->db->prepare($prevQuery);
				$prevResult->execute();
				if($prevResult->rowCount() > 0){
					//Update user data if already exists
					$query = "UPDATE ".$this->userTbl." 
					SET 
					users_email = '".$userData['email']."',
					users_gp_link = '".$userData['link']."',
					users_modified_date = '".date("Y-m-d H:i:s")."'
					WHERE 
					users_oauth_provider = '".$userData['oauth_provider']."' 
					AND 
					users_oauth_uid = '".$userData['oauth_uid']."'";
					$update = $this->db->prepare($query)->execute();
				}else{
					//Insert user data
					$query = "INSERT INTO ".$this->userTbl." 
					SET 
					users_oauth_provider = '".$userData['oauth_provider']."',
					users_oauth_uid = '".$userData['oauth_uid']."',
					users_name = '".$userData['first_name']."',
					users_surname = '".$userData['last_name']."',
					users_email = '".$userData['email']."',
					users_gender = '".$userData['gender']."',
					users_provider_locale = '".$userData['locale']."',
					users_picture = '".$userData['picture']."',
					users_up_picture = 'img/top-header1.jpg',
					users_gp_link = '".$userData['link']."',
					users_created_date = '".date("Y-m-d H:i:s")."',
					users_modified_date = '".date("Y-m-d H:i:s")."'";
					$insert = $this->db->prepare($query)->execute();
				}
				
				//Get user data from the database
				$result = $this->db->prepare($prevQuery);
				$result->execute();
				$userData = $result->fetch(PDO::FETCH_ASSOC);
			}
			
			//Return user data
			return $userData;
		}
	}
	?>
