$("#kategoriEkleBtn").on("click", function() {
    var data = $("#kategoriEkleForm").serialize();
    $.ajax({
        url: "ayarlar/islem.php?islem=kategoriEkle",
        type: "POST",
        data: data,
        success: function(cevap) {
            $("#kategoriEkleAlert").html(cevap).hide().fadeIn(700);
        }
    });
});

$("#kategoriGuncelleBtn").on("click", function() {
    var data = $("#kategoriGuncelleForm").serialize();
    $.ajax({
        url: "ayarlar/islem.php?islem=kategoriGuncelle",
        type: "POST",
        data: data,
        success: function(cevap) {
            $("#kategoriGuncelleAlert").html(cevap).hide().fadeIn(700);
        }
    });
});

$("#yGiris").on("click", function() {
    var data = $("#ygirisForm").serialize();
    $.ajax({
        url: "ayarlar/islem.php?islem=yGiris",
        type: "POST",
        data: data,
        success: function(cevap) {
            $("#yGirisAlert").html(cevap).hide().fadeIn(700);
        }
    });
});

function ParaFormat(Num) {

    Num += '';

    Num = Num.replace('.', '');
    Num = Num.replace('.', '');
    Num = Num.replace('.', '');

    Num = Num.replace('.', '');
    Num = Num.replace('.', '');
    Num = Num.replace('.', '');

    x = Num.split(',');

    x1 = x[0];

    x2 = x.length > 1 ? ',' + x[1] : '';

    var rgx = /(\d+)(\d{3})/;

    while (rgx.test(x1))

        x1 = x1.replace(rgx, '$1' + '.' + '$2');

    return x1 + x2;

}

$("#urunEkleBtn").on("click", function() {
    var data = new FormData($('#urunEkleForm')[0]);
    $.ajax({
        url: "ayarlar/islem.php?islem=urunekle",
        type: "POST",
        data: data,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function(cevap) {
            $("#urunEkleAlert").html(cevap).hide().fadeIn(700);
        }
    });
});

$("#urunGuncelleBtn").on("click", function() {
    var data = new FormData($('#urunGuncelleForm')[0]);
    $.ajax({
        url: "ayarlar/islem.php?islem=urunGuncelle",
        type: "POST",
        data: data,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function(cevap) {
            $("#urunGuncelleAlert").html(cevap).hide().fadeIn(700);
        }
    });
});

$("#profilGuncelleBtn").on("click", function() {
    var data = $("#profilGuncelleForm").serialize();
    $.ajax({
        url: "boss/root/islem.php?islem=profilGuncelle",
        type: "POST",
        data: data,
        success: function(cevap) {
            $("#profilGuncelleAlert").html(cevap).hide().fadeIn(700);
        }
    });
});

$("#hobiGuncelleBtn").on("click", function() {
    var data = $("#hobiGuncelleForm").serialize();
    $.ajax({
        url: "boss/root/islem.php?islem=hobiGuncelle",
        type: "POST",
        data: data,
        success: function(cevap) {
            $("#hobiGuncelleAlert").html(cevap).hide().fadeIn(700);
        }
    });
});

$("#blogEkleBtn").on("click", function() {
    for (instance in CKEDITOR.instances) CKEDITOR.instances[instance].updateElement();
    var data = new FormData($('#blogEkleForm')[0]);
    $.ajax({
        url: "boss/root/islem.php?islem=blogekle",
        type: "POST",
        data: data,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function(cevap) {
            $("#blogEkleAlert").html(cevap).hide().fadeIn(700);
        }
    });
});

$("#yorumEkleBtn").on("click", function() {
    var data = $("#yorumEkleForm").serialize();
    $.ajax({
        url: "boss/root/islem.php?islem=yorumekle",
        type: "POST",
        data: data,
        success: function(cevap) {
            $("#yorumEkleAlert").html(cevap).hide().fadeIn(700);
        }
    });
});

$("#durumEkleBtn").on("click", function() {
    var data = $("#durumEkleForm").serialize();
    $.ajax({
        url: "boss/root/islem.php?islem=durumekle",
        type: "POST",
        data: data,
        success: function(cevap) {
            $("#durumEkleAlert").html(cevap).hide().fadeIn(700);
        }
    });
});

$("#kapakEkleBtn").on("click", function() {
    var data = new FormData($('#kapakEkleForm')[0]);
    $.ajax({
        url: "boss/root/islem.php?islem=kapakekle",
        type: "POST",
        data: data,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function(cevap) {
            $("#kapakEkleAlert").html(cevap).hide().fadeIn(700);
            alert("İşlem gerçekleşti");
        }
    });
});