<meta charset="utf-8">
<?php

require_once 'baglanti.php';
require_once 'class.upload.php';
require_once 'function.php';

// KATEGORİ İŞLEMLERİ
if (g('islem') == 'kategoriEkle') {
    $kategori_ust = p('kategori_ust');
    $kategori_title = p('kategori_title');
    $kategori_desc = p('kategori_desc');
    $kategori_keyw = p('kategori_keyw');
    $kategori_durum = p('kategori_durum');


    if (empty($kategori_ust)) {
        echo "<div class='alert alert-warning'>Lütfen Kategori Modelinizi Seçiniz</div>";
    } elseif (empty($kategori_title)) {
        echo "<div class='alert alert-warning'>Lütfen kategorinize bir isim belirleyin</div>";
    } elseif (empty($kategori_desc)) {
        echo "<div class='alert alert-warning'>Lütfen kategorinize bir açıklama giriniz</div>";
    } elseif (empty($kategori_keyw)) {
        echo "<div class='alert alert-warning'>Lütfen kategorinize anahtar kelime giriniz</div>";
    } else {
        $key = rand(99, 999);
        $key .= rand(99, 999);
        $key .= rand(99, 999);
        $key .= rand(99, 999);
        $key .= rand(99, 999);
        $key .= rand(99, 999);
        $ekle = $db->prepare("INSERT INTO kategoriler SET kategori_key=?, kategori_title=?, kategori_desc=?, kategori_keyw=?, kategori_ust=?, kategori_durum=?");
        $ekleme = $ekle->execute(array($key, $kategori_title, $kategori_desc, $kategori_keyw, $kategori_ust, $kategori_durum));
        if ($ekleme) {
            echo "<div class='alert alert-success'>Kategori başarılı bir şekilde eklendi.</div><meta http-equiv=\"refresh\" content=\"1;URL=kategoriler.php\">";

        } else {
            echo "<div class='alert alert-danger'>Veritabanınızın bakıma ihtiyacı var.</div>";
        }
    }
}

if (g('kategoriSil') == 'ok') {
    $sil = $db->prepare("DELETE FROM kategoriler WHERE kategori_id=?");
    $silme = $sil->execute(array(g('kategori_id')));
    if ($silme) {
        git("../kategoriler.php?islem=tamamlandi");
    } else {
        git("../kategoriler.php?islem=problem");
    }
}

if (g('islem') == 'kategoriGuncelle') {
    $kategori_ust = p('kategori_ust');
    $kategori_title = p('kategori_title');
    $kategori_desc = p('kategori_desc');
    $kategori_keyw = p('kategori_keyw');
    $kategori_durum = p('kategori_durum');
    $kategori_id = p('kategori_id');


    if (empty($kategori_ust)) {
        echo "<div class='alert alert-warning'>Lütfen Kategori Modelinizi Seçiniz</div>";
    } elseif (empty($kategori_title)) {
        echo "<div class='alert alert-warning'>Lütfen kategorinize bir isim belirleyin</div>";
    } elseif (empty($kategori_desc)) {
        echo "<div class='alert alert-warning'>Lütfen kategorinize bir açıklama giriniz</div>";
    } elseif (empty($kategori_keyw)) {
        echo "<div class='alert alert-warning'>Lütfen kategorinize anahtar kelime giriniz</div>";
    } else {
        $guncelle = $db->prepare("UPDATE kategoriler SET kategori_title=?, kategori_desc=?, kategori_keyw=?, kategori_ust=?, kategori_durum=? WHERE kategori_id='$kategori_id' ");
        $guncelleme = $guncelle->execute(array($kategori_title, $kategori_desc, $kategori_keyw, $kategori_ust, $kategori_durum));
        if ($guncelleme) {
            echo "<div class='alert alert-success'>Kategori başarılı bir şekilde eklendi.</div><meta http-equiv=\"refresh\" content=\"1;URL=kategoriler.php\">";
        } else {
            echo "<div class='alert alert-danger'>Veritabanınızın bakıma ihtiyacı var.</div>";
        }
    }
}

// SESSION İŞLEMLERİ

if (g('islem') == 'yGiris') {

    $eposta = p('yEposta');
    $sifre = p('ySifre');
    $toplam = p('yToplam');
    $dkodu = p('dKodu');

    if (empty($eposta)) {
        echo "<div class='alert alert-warning'>Lütfen E-Posta adresinizi giriniz.</div>";
    } elseif (filter_var($eposta, FILTER_VALIDATE_EMAIL) != true) {
        echo "<div class='alert alert-warning'>Lütfen geçerli bir E-Posta adresi giriniz.</div>";
    } elseif (empty($sifre)) {
        echo "<div class='alert alert-warning'>Lütfen şifrenizi giriniz.</div>";
    } elseif ($toplam != md5($dkodu)) {
        echo "<div class='alert alert-warning'>Doğrulama kodunu hatalı girdiniz.</div>";
    } else {
        $veri = $db->prepare("SELECT * FROM yonetim WHERE yonetim_eposta=? AND yonetim_sifre=?");
        $veri->execute(array($eposta, md5($sifre)));
        $v = $veri->fetchAll(PDO::FETCH_ASSOC);
        $say = $veri->rowCount();
        foreach ($v as $yonetim) ;
        if ($say) {
            if ($yonetim['yonetim_yetki'] != '1') {
                echo "<div class='alert alert-warning'>Giriş yetkiniz bulunmamaktadır.</div>";
            } else {
                $_SESSION['id'] = $yonetim['yonetim_id'];
                $_SESSION['isim'] = $yonetim['yonetim_isim'];
                $_SESSION['soyisim'] = $yonetim['yonetim_soyisim'];
                $_SESSION['eposta'] = $yonetim['yonetim_eposta'];
                $_SESSION['yetki'] = $yonetim['yonetim_yetki'];
                echo "<div class='alert alert-success'>Giriş başarıyla gerçekleşti, lütfen bekleyiniz.</div><meta http-equiv='refresh' content='2;URL=index.php'>";
            }
        } else {
            echo "E-Posta yada şifrenizi yanlış girdiniz. Lütfen kontrol ediniz.";
        }
    }

}

if (g('islem') == 'cikis') {
    session_destroy();
    header("Location:../giris.php");

}

// ÜRÜN İŞLEMLERİ

// ürün ekle
if (g('islem') == 'urunekle') {

    /* STANDART RESİM YÜKLEME İŞLEMİ
       if ($_FILES['urun_resim']["size"] > (1024 * 1024) * 2) {

         echo "Resim boyutu maksimum 2 mb olmalı";

     } else {
         $uploads_dir = '../resimler';
         @$tmp_name = $_FILES['urun_resim'] ["tmp_name"];
         @$name = $_FILES['urun_resim'] ['name'];
         $rd = rand(1000, 9999);
         $rd .= rand(1000, 9999);
         $rd .= rand(1000, 9999);
         $rd .= rand(1000, 9999);
         $rd .= rand(1000, 9999);
         $tire = "-";
         $resimyolu = substr($uploads_dir, 6) . "/" . $rd . $name;
         $yukle = @move_uploaded_file($tmp_name, "$uploads_dir/$rd$tire$name");

     }

     */

    @$name = $_FILES['urun_resim']['name'];
    $yol = '../resimler';
    $rn = resimadi();
    $uzanti = uzanti($name);
    $dbyol = "resimler/$rn.$uzanti";


    if ($_FILES['urun_resim']["size"] > (1024 * 1024) * 2) {
        echo "Resim boyutu maksimum 2 mb olmalı";
    } else {
        $resimyukleme = resimyukle('urun_resim', $rn, $yol);
        if ($resimyukleme) {
            $urun_kategori = p('urun_kategori');
            $urun_title = p('urun_title');
            $urun_desc = p('urun_desc');
            $urun_meta_desc = p('urun_meta_desc');
            $urun_meta_title = p('urun_meta_title');
            $urun_meta_keyw = p('urun_meta_keyw');
            $urun_fiyat = p('urun_fiyat');
            $urun_sira = p('urun_sira');
            $ekle = $db->prepare("INSERT INTO urunler SET urun_resim=?,urun_title=?,urun_desc=?,urun_meta_title=?,urun_meta_desc=?,urun_meta_keyw=?,urun_fiyat=?,urun_kategori=?,urun_sira=?");
            $ekleme = $ekle->execute(array($dbyol, $urun_title, $urun_desc, $urun_meta_title, $urun_meta_desc, $urun_meta_keyw, noktasil($urun_fiyat), $urun_kategori, $urun_sira));
            if ($ekleme) {
                echo "TAMAMDIR";
            } else {
                echo "sıkıntıvar";
            }
        } else {
            echo 'Resim yüklenirken bir hata oluştu. Veritabanınızın bakıma ihtiyacı var.';
        }
    }


}

// ürün sil
if (g('urunSil') == 'ok') {
    $u = urungetir(g('urun_id'));
    foreach ($u as $urun) ;
    $eskiresim = '../' . $urun['urun_resim'];
    $sil = $db->prepare("DELETE FROM urunler WHERE urun_id=?");
    $silme = $sil->execute(array(g('urun_id')));
    if ($silme) {
        unlink($eskiresim);
        git("../urunler.php?islem=tamamlandi");
    } else {
        git("../urunler.php?islem=problem");
    }
}

// ürün güncelle
if (g('islem') == 'urunGuncelle') {
    degerlerigor();

    $urun_id = p('urun_id');
    $urun_kategori = p('urun_kategori');
    $urun_title = p('urun_title');
    $urun_desc = p('urun_desc');
    $urun_meta_desc = p('urun_meta_desc');
    $urun_meta_title = p('urun_meta_title');
    $urun_meta_keyw = p('urun_meta_keyw');
    $urun_fiyat = p('urun_fiyat');
    $urun_sira = p('urun_sira');
    $uruneskiresim = urunresimgetir($urun_id);
    echo $uruneskiresim;
    if (empty($urun_kategori)) {
        echo "Lütfen kategori seçimi yapınız";
    } elseif (empty($urun_title)) {
        echo "Lütfen ürün başlığı giriniz";
    } elseif (empty($urun_desc)) {
        echo "Lütfen ürün açıklaması giriniz";
    } elseif (empty($urun_meta_title)) {
        echo "Lütfen ürün meta başlığı giriniz";
    } elseif (empty($urun_meta_desc)) {
        echo "Lütfen ürün meta açıklaması giriniz";
    } elseif (empty($urun_meta_keyw)) {
        echo "Lütfen meta keyword giriniz";
    } elseif (empty($urun_fiyat)) {
        echo "Lütfen fiyatını giriniz";
    } else {

        if ($_FILES['urun_resim']["size"] <= 0) {
            $guncelle = $db->prepare("UPDATE urunler SET urun_resim=?,urun_title=?,urun_desc=?,urun_meta_title=?,urun_meta_desc=?,urun_meta_keyw=?,urun_fiyat=?,urun_kategori=?,urun_sira=? WHERE urun_id='$urun_id'");
            $guncelleme = $guncelle->execute(array($uruneskiresim, $urun_title, $urun_desc, $urun_meta_title, $urun_meta_desc, $urun_meta_keyw, noktasil($urun_fiyat), $urun_kategori, $urun_sira));
            if ($guncelleme) {
                echo "<div class='alert alert-success'>Kategori başarılı bir şekilde eklendi.</div><meta http-equiv=\"refresh\" content=\"1;URL=urunler.php\">";
            } else {
                echo "<div class='alert alert-danger'>Veritabanınızın bakıma ihtiyacı var.</div>";
            }
        } elseif ($_FILES['urun_resim']["size"] > (1024 * 1024) * 2) {
            echo "Resim boyutu maksimum 2 mb olmalı";
        } else {
            @$name = $_FILES['urun_resim']['name'];
            $yol = '../resimler';
            $rn = resimadi();
            $uzanti = uzanti($name);
            $dbyol = "resimler/$rn.$uzanti";
            $resimyukleme = resimyukle('urun_resim', $rn, $yol);
            if ($resimyukleme) {
                $guncelle = $db->prepare("UPDATE urunler SET urun_resim=?,urun_title=?,urun_desc=?,urun_meta_title=?,urun_meta_desc=?,urun_meta_keyw=?,urun_fiyat=?,urun_kategori=?,urun_sira=? WHERE urun_id='$urun_id'");
                $guncelleme = $guncelle->execute(array($dbyol, $urun_title, $urun_desc, $urun_meta_title, $urun_meta_desc, $urun_meta_keyw, noktasil($urun_fiyat), $urun_kategori, $urun_sira));
                if ($guncelleme) {
                    echo "<div class='alert alert-success'>Kategori başarılı bir şekilde eklendi.</div><meta http-equiv=\"refresh\" content=\"1;URL=urunler.php\">";
                    unlink("../$uruneskiresim");
                } else {
                    echo "<div class='alert alert-danger'>Veritabanınızın bakıma ihtiyacı var.</div>";
                }
            }

        }
    }


}


// Profil Güncelle

if (g('islem') == 'profilGuncelle') {
    degerlerigor();

    $users_id = p('users_id');
    $users_idcode = p('users_idcode');
    $users_email = p('users_email');
    $users_name = p('users_name');
    $users_surname = p('users_surname');
    $users_birth = p('users_birth');
    $users_city = p('users_city');
    $users_phone = p('users_phone');
    $users_school = p('users_school');
    $users_job = p('users_job');
    $users_shortdesc = p('users_shortdesc');
    $users_youtube = p('users_youtube');
    $users_facebook = p('users_facebook');
    $users_twitter = p('users_twitter');
    $users_instagram = p('users_instagram');
    $users_website = p('users_website');
    $users_gender = p('users_gender');
    $users_provider_locale = p('users_provider_locale');


    $guncelle = $db->prepare("
UPDATE users SET 
users_idcode=?,
users_email=?,
users_name=?,
users_surname=?,
users_birth=?,
users_phone=?,
users_school=?,
users_city=?,
users_job=?,
users_shortdesc=?,
users_youtube=?,
users_facebook=?,
users_twitter=?,
users_instagram=?,
users_website=?,
users_gender=?,
users_provider_locale=?
 WHERE users_id='$users_id'");
    $guncelleme = $guncelle->execute(array(
        $users_idcode,
        $users_email,
        $users_name,
        $users_surname,
        $users_birth,
        $users_phone,
        $users_school,
        $users_city,
        $users_job,
        $users_shortdesc,
        $users_youtube,
        $users_facebook,
        $users_twitter,
        $users_instagram,
        $users_website,
        $users_gender,
        $users_provider_locale
    ));
    if ($guncelleme) {
        echo "<div class='alert alert-success'>Kategori başarılı bir şekilde eklendi.</div>";
    } else {
        echo "<div class='alert alert-danger'>Veritabanınızın bakıma ihtiyacı var.</div>";
    }

}

// Hobi Güncelle

if (g('islem') == 'hobiGuncelle') {
    degerlerigor();

    $users_id = p('users_id');
    $users_fav_hobbies = p('users_fav_hobbies');
    $users_fav_series = p('users_fav_series');
    $users_fav_movies = p('users_fav_movies');
    $users_fav_sports = p('users_fav_sports');
    $users_fav_games = p('users_fav_games');
    $users_fav_musics = p('users_fav_musics');
    $users_fav_books = p('users_fav_books');
    $users_fav_journal = p('users_fav_journal');
    $users_fav_events = p('users_fav_events');
    $users_other_hobbies = p('users_other_hobbies');


    $guncelle = $db->prepare("
UPDATE users SET 
users_fav_hobbies=?,
users_fav_series=?,
users_fav_movies=?,
users_fav_sports=?,
users_fav_games=?,
users_fav_musics=?,
users_fav_books=?,
users_fav_journal=?,
users_fav_events=?,
users_other_hobbies=?
 WHERE users_id='$users_id'");
    $guncelleme = $guncelle->execute(array(
        $users_fav_hobbies,
        $users_fav_series,
        $users_fav_movies,
        $users_fav_sports,
        $users_fav_games,
        $users_fav_musics,
        $users_fav_books,
        $users_fav_journal,
        $users_fav_events,
        $users_other_hobbies
    ));
    if ($guncelleme) {
        echo "<div class='alert alert-success'>Kategori başarılı bir şekilde eklendi.</div>";
    } else {
        echo "<div class='alert alert-danger'>Veritabanınızın bakıma ihtiyacı var.</div>";
    }

}


if (g('islem') == 'blogekle') {
    /*degerlerigor();*/
     /* STANDART RESİM YÜKLEME İŞLEMİ
        if ($_FILES['urun_resim']["size"] > (1024 * 1024) * 2) {

          echo "Resim boyutu maksimum 2 mb olmalı";

      } else {
          $uploads_dir = '../resimler';
          @$tmp_name = $_FILES['urun_resim'] ["tmp_name"];
          @$name = $_FILES['urun_resim'] ['name'];
          $rd = rand(1000, 9999);
          $rd .= rand(1000, 9999);
          $rd .= rand(1000, 9999);
          $rd .= rand(1000, 9999);
          $rd .= rand(1000, 9999);
          $tire = "-";
          $resimyolu = substr($uploads_dir, 6) . "/" . $rd . $name;
          $yukle = @move_uploaded_file($tmp_name, "$uploads_dir/$rd$tire$name");

      }

      */

    @$name = $_FILES['blog_picture']['name'];
    $yol = '../../img/blog';
    $rn = resimadi();
    $uzanti = uzanti($name);
    $dbyol = "img/blog/$rn.$uzanti";


    if ($_FILES['blog_picture']["size"] > (1024 * 1024) * 2) {
        echo "Resim boyutu maksimum 2 mb olmalı";
    } else {
        $resimyukleme = resimyukle('blog_picture', $rn, $yol); // blog-ekle php yi açsana
        if ($resimyukleme) {
            $blog_user_id = p('blog_user_id');
            $blog_user_picture = p('blog_user_picture');
            $blog_user_name = p('blog_user_name');
            $blog_date = date("Y-m-d H:i:s");
            $blog_category = p('blog_category');
            $blog_title = p('blog_title');
            $blog_text = $_POST['blog_text'];
            $blog_keyw = p('blog_keyw');
            $veri_turu = "blog";
            $ekle = $db->prepare("
                INSERT INTO blog SET 
                blog_picture=?,
                blog_user_id=?,
                blog_user_picture=?,
                blog_user_name=?,
                blog_date=?,
                blog_category=?,
                blog_title=?,
                blog_text=?,
                blog_keyw=?,
                veri_turu=?
                          ");
            $ekleme = $ekle->execute(array(
                $dbyol,
                $blog_user_id,
                $blog_user_picture,
                $blog_user_name,
                $blog_date,
                $blog_category,
                $blog_title,
                $blog_text,
                $blog_keyw,
                $veri_turu));
            if ($ekleme) echo "TAMAMDIR";
            else echo "sıkıntıvar";

        } else {
            echo 'Resim yüklenirken bir hata oluştu. Veritabanınızın bakıma ihtiyacı var.';
        }
    }


}

// Blog Yorum Ekleme İşlemleri
if (g('islem') == 'yorumekle') {
    degerlerigor();
    $comments_user_id = p('comments_user_id');
    $comments_child_id = p('comments_child_id');
    $comments_user_picture = p('comments_user_picture');
    $comments_user_name = p('comments_user_name');
    $comments_date = p('comments_date');
    $comments_blog_id = p('comments_blog_id');
    $comments_text = p('comments_text');

        $ekle = $db->prepare("INSERT INTO comments SET comments_user_id=?, comments_child_id=?, comments_user_picture=?, comments_user_name=?, comments_date=?, comments_blog_id=?, comments_text=?");
        $ekleme = $ekle->execute(array( $comments_user_id, $comments_child_id, $comments_user_picture, $comments_user_name, $comments_date, $comments_blog_id, $comments_text));
        if ($ekleme) {
            echo "<div class='alert alert-success'>Yorum başarılı bir şekilde eklendi.";

        } else {
            echo "<div class='alert alert-danger'>Veritabanınızın bakıma ihtiyacı var.</div>";
        }
    }


    // Blog Yorum Beğenme İşlemleri
if (g('islem') == 'blogYorumBegen') {

    $likes_user_id = p('likes_user_id');
    $likes_comments_id = p('likes_comments_id');
    $likes_user_name = p('likes_user_name');
    $likes_date = p('likes_date');
    $likes_state = p('likes_state');

    $likes = $db->query("SELECT * FROM likes WHERE likes_comments_id=$likes_comments_id AND likes_user_id=$likes_user_id");
    $likesAl = $likes->fetchAll(PDO::FETCH_ASSOC);
    foreach ($likesAl as $likesBas){

    }
    $likeId =$likesBas['likes_id'];

if($likesBas['likes_comments_id'] == $likes_comments_id && $likesBas['likes_user_id'] == $likes_user_id){

    $ekle = $db->prepare("UPDATE likes SET likes_state=? WHERE likes_id='$likeId'");
    $ekleme = $ekle->execute(array( $likes_state));
    if ($ekleme) {
        echo "<div class='alert alert-success'>Yorum başarılı bir şekilde eklendi.";

    } else {
        echo "<div class='alert alert-danger'>Veritabanınızın bakıma ihtiyacı var.</div>";
    }

} else{

    $ekle = $db->prepare("INSERT INTO likes SET likes_user_id=?, likes_comments_id=?, likes_user_name=?, likes_date=?, likes_state=?");
    $ekleme = $ekle->execute(array( $likes_user_id, $likes_comments_id, $likes_user_name, $likes_date, $likes_state));
    if ($ekleme) {
        echo "<div class='alert alert-success'>Yorum başarılı bir şekilde eklendi.";

    } else {
        echo "<div class='alert alert-danger'>Veritabanınızın bakıma ihtiyacı var.</div>";
    }

}
        
    }

    // Blog Yorum Beğenme İşlemleri
    if (g('islem') == 'blogBegen') {
            $likes_id = p('likes_id');       
            $likes_user_id = p('likes_user_id');
            $likes_blog_id = p('likes_blog_id');
            $likes_user_name = p('likes_user_name');
            $likes_date = p('likes_date');
            $likes_state = p('likes_state');
        
            $likes = $db->query("SELECT * FROM likes WHERE likes_blog_id=$likes_blog_id AND likes_user_id=$likes_user_id");
            $likesAl = $likes->fetchAll(PDO::FETCH_ASSOC);
            foreach ($likesAl as $likesBas){
        
            }
            $likeId =$likesBas['likes_id'];
            
        if($likes_state == 0 && $likesBas['likes_blog_id'] == $likes_blog_id && $likesBas['likes_user_id'] == $likes_user_id){        
            $sil = $db->prepare("DELETE FROM likes WHERE likes_id=$likes_id");
            $silme = $sil->execute(array(g('likes_id')));
            if ($silme) {
                echo "<div class='alert alert-success'>Yorum başarılı bir şekilde eklendi.";        
            } else {
                echo "<div class='alert alert-danger'>Veritabanınızın bakıma ihtiyacı var.</div>";
            }
        
        } else{
        
            $ekle = $db->prepare("INSERT INTO likes SET likes_user_id=?, likes_blog_id=?, likes_user_name=?, likes_date=?, likes_state=?");
            $ekleme = $ekle->execute(array( $likes_user_id, $likes_blog_id, $likes_user_name, $likes_date, $likes_state));
            if ($ekleme) {
                echo "<div class='alert alert-success'>Yorum başarılı bir şekilde eklendi.";
        
            } else {
                echo "<div class='alert alert-danger'>Veritabanınızın bakıma ihtiyacı var.</div>";
            }
        
        }
                
            }


//Durum Ekleme İşlemleri
if (g('islem') == 'durumekle') {

    $status_user_id = p('status_user_id');
    $status_user_picture = p('status_user_picture');
    $status_user_name = p('status_user_name');
    $status_date = date("Y-m-d H:i:s");
    $status_text = p('status_text');
    $veri_turu = "durum";
        $ekle = $db->prepare("INSERT INTO userstatus SET veri_turu=?, status_user_id=?, status_user_picture=?, status_user_name=?, status_date=?, status_text=?");
        $ekleme = $ekle->execute(array( $veri_turu, $status_user_id, $status_user_picture, $status_user_name, $status_date, $status_text));
        if ($ekleme) {
            echo "<div class='alert alert-success'>Yorum başarılı bir şekilde eklendi.";

        } else {
            echo "<div class='alert alert-danger'>Veritabanınızın bakıma ihtiyacı var.</div>";
        }
    }
  


    //Status Silme
    if (g('islem') == 'statussil') {
        $status_id = p('status_id');
        $sil = $db->prepare("DELETE FROM userstatus WHERE status_id='$status_id'");
        $silme = $sil->execute(array(g('status_id')));
        if ($silme) {
            git("../kategoriler.php?islem=tamamlandi");
        } else {
            git("../kategoriler.php?islem=problem");
        }
    }

        //Blog Silme
        if (g('islem') == 'blogsil') {
            $blog_id = p('blog_id');
            $sil = $db->prepare("DELETE FROM blog WHERE blog_id='$blog_id'");
            $silme = $sil->execute(array(g('blog_id')));

        }



// Kapak Resim Güncelle

if (g('islem') == 'kapakekle') {
    $users_id = p('users_id'); 
    $eskikapakresmi = kapakresimgetir($users_id);
    $eskikucukkapakresmi = kucukkapakresimgetir($users_id);
            @$name = $_FILES['users_up_picture']['name'];
            $yol = '../../img/users/uppicture';
            $rn = resimadi();
            $uzanti = uzanti($name);
            $dbyol = "img/users/uppicture/$rn.$uzanti";
            $kucukYol = '../../img/users/uppicture/sml/';
            $kucukdbyol = "img/users/uppicture/sml/$rn.$uzanti";
            $resimyukleme = kapakYukle('users_up_picture', $rn, $yol);
            if ($resimyukleme){
                $guncelle = $db->prepare("UPDATE users SET users_up_picture=?, users_up_sml_picture=?  WHERE users_id=$users_id");
                $guncelleme = $guncelle->execute(array($dbyol,$kucukdbyol));
                if ($guncelleme) {
                    echo "<div class='alert alert-success'>Kategori başarılı bir şekilde eklendi.</div>";
               if($eskikapakresmi!="img/users/uppicture/default.jpg" && $eskikucukkapakresmi !="img/sml/uppicture/default.jpg"){unlink("../../$eskikapakresmi");unlink("../../$eskikucukkapakresmi");}
                } else {
                    echo "<div class='alert alert-danger'>Veritabanınızın bakıma ihtiyacı var.</div>";
                }
            }else{
                echo "<meta http-equiv=\"refresh\" content=\"1;URL=profile-settings.php\">";
            }

        }
    




?>