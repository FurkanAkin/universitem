<?php

$host = "localhost";
$dbname = "universitem";
$kullanici = "root";
$sifre = "";

try {
    $db = new PDO(  "mysql:host=$host;dbname=$dbname",
                    $kullanici, $sifre,
        array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
        )
        );

} catch ( PDOException $e) {
    print $e->getMessage();
}
