<?php

function temiz($text)
{
    $text = strip_tags($text);
    $text = preg_replace('/<a\s+.*?href="([^"]+)"[^>]*>([^<]+)<\/a>/is', '\2 (\1)', $text);
    $text = preg_replace('/<!--.+?-->/', '', $text);
    $text = preg_replace('/{.+?}/', '', $text);
    $text = preg_replace('/&nbsp;/', ' ', $text);
    $text = preg_replace('/&amp;/', ' ', $text);
    $text = preg_replace('/&quot;/', ' ', $text);
    $text = htmlspecialchars($text);
    $text = addslashes($text);
    return $text;
}

function g($par)
{
    $par = temiz(@$_GET[$par]);
    return $par;
}
function a($par)
{
    $par = temiz(@$_POST[$par]);
    return $par;
}
function p($par)
{
    $par = htmlspecialchars(trim($_POST[$par]));
    return $par;
}

function git($par)
{
    header("Location:{$par}");
}

function s($par)
{
    $session = $_SESSION[$par];
    return $session;
}

function yoneticikontrol()
{
    if (!$_SESSION || !s('yetki') == '1') {
        header("Location:giris.php");
    }
}

function rasgeleharf($kackarakter)
{
    $s = "";
    $char = "ABCDEFGHIJKLMNOPRSTUVWYZQX";
    for ($k = 1; $k <= $kackarakter; $k++) {
        $h = substr($char, mt_rand(0, strlen($char) - 1), 1);
        $s .= $h;
    }
    return $s;
}

function uzanti($dosya)
{
    $uzanti = pathinfo($dosya);
    $uzanti = $uzanti["extension"];
    return $uzanti;
}

function resimadi()
{
    $rn = rand(1000, 9999);
    $rn .= rasgeleharf(1);
    $rn .= rand(1000, 9999);
    $rn .= rasgeleharf(2);
    $rn .= rasgeleharf(2);
    $rn .= rand(1000, 9999);
    $rn .= rasgeleharf(1);
    $rn .= rand(1000, 9999);
    $rn .= rasgeleharf(2);
    $rn .= rasgeleharf(2);
    $rn .= rand(1000, 9999);
    $rn .= rasgeleharf(1);
    $rn .= rand(1000, 9999);
    $rn .= rasgeleharf(2);
    $rn .= rasgeleharf(2);
    $rn .= rand(1000, 9999);
    $rn .= rasgeleharf(1);
    $rn .= rand(1000, 9999);
    $rn .= rasgeleharf(2);
    $rn .= rasgeleharf(2);
    $rn .= rand(1000, 9999);
    $rn .= rasgeleharf(1);
    return $rn;
}

function parayaz($para)
{
    $para = number_format($para, 2, ',', '.') . " ₺";
    return $para;
}

function parayaz2($para)
{
    $para = number_format($para, 2, ',', '.');
    return $para;
}

function noktasil($s)
{
    $tr = array('.', ',');
    $eng = array('', '.');
    $s = str_replace($tr, $eng, $s);
    return $s;
}

function resimyukle($postisim, $yeniisim, $yol)
{
    // VEROT RESİM YÜKLEME
    $foo = new Upload($_FILES[$postisim]);
    if ($foo->uploaded) {
        $foo->allowed = array('image/*');
        $foo->file_new_name_body = $yeniisim;
        $foo->image_resize = true;
        $foo->image_ratio_crop  = true;
        $foo->image_x = 1920;
        $foo->image_y = 640;
        $foo->Process($yol);
        if ($foo->processed) {
            $foo->Clean();
            return true;
        } else {
            return false;
        }
    }


    // VEROT RESİM YÜKLEME
}

function kullanicigetir($par)
{
    Global $db;

    $veri = $db->prepare("SELECT * FROM users WHERE users_id=$par");
    $veri->execute(array());
    $v = $veri->fetchAll(PDO::FETCH_ASSOC);
    return $v;


}

function urunresimgetir($id)
{
    Global $db;
    $veri = $db->prepare("SELECT urun_resim FROM urunler WHERE urun_id='$id'");
    $veri->execute(array());
    $v = $veri->fetchAll(PDO::FETCH_ASSOC);
    foreach ($v as $ur) ;
    return $ur['urun_resim'];
}

function degerlerigor()
{
    echo "<pre>";
    print_r($_POST);
    echo "</pre>";
}

function profilduzenlebossa($par)
{
    if ($users[$par] || '') {
        echo "is-empty";
    } else {
        echo "";
    }

}

function tcyoksa($users_id)
{
    Global $db;
    foreach (kullanicigetir($users_id) as $users) ;
    $user_tc = $users['users_idcode'];
    if (!$user_tc) header('Location:/profile-settings.php?idcode_empty');

}

function sessionyoksaindexegit(){
    if (empty($_SESSION)){
        header('Location:index.php');
    }
}

function sessionVarsaKullaniciGetir(){
    if ($_SESSION){
        $users_id = $userData['users_id'];
        foreach (kullanicigetir($users_id) as $users);
        tcyoksa($users_id);
    }
}

function iller($a)
{
    $iller = array('Belirtilmedi','Adana', 'Adıyaman', 'Afyon', 'Ağrı', 'Amasya', 'Ankara', 'Antalya', 'Artvin',
										'Aydın', 'Balıkesir', 'Bilecik', 'Bingöl', 'Bitlis', 'Bolu', 'Burdur', 'Bursa', 'Çanakkale',
										'Çankırı', 'Çorum', 'Denizli', 'Diyarbakır', 'Edirne', 'Elazığ', 'Erzincan', 'Erzurum', 'Eskişehir',
										'Gaziantep', 'Giresun', 'Gümüşhane', 'Hakkari', 'Hatay', 'Isparta', 'Mersin', 'İstanbul', 'İzmir', 
										'Kars', 'Kastamonu', 'Kayseri', 'Kırklareli', 'Kırşehir', 'Kocaeli', 'Konya', 'Kütahya', 'Malatya', 
										'Manisa', 'Kahramanmaraş', 'Mardin', 'Muğla', 'Muş', 'Nevşehir', 'Niğde', 'Ordu', 'Rize', 'Sakarya',
										'Samsun', 'Siirt', 'Sinop', 'Sivas', 'Tekirdağ', 'Tokat', 'Trabzon', 'Tunceli', 'Şanlıurfa', 'Uşak',
										'Van', 'Yozgat', 'Zonguldak', 'Aksaray', 'Bayburt', 'Karaman', 'Kırıkkale', 'Batman', 'Şırnak',
										'Bartın', 'Ardahan', 'Iğdır', 'Yalova', 'Karabük', 'Kilis', 'Osmaniye', 'Düzce');
    return $iller[$a];
}


function kapakresimgetir($id)
{
    Global $db;
    $veri = $db->prepare("SELECT users_up_picture FROM users WHERE users_id='$id'");
    $veri->execute(array());
    $v = $veri->fetchAll(PDO::FETCH_ASSOC);
    foreach ($v as $kapak){
    return $kapak['users_up_picture'];
}
}

function kucukkapakresimgetir($id)
{
    Global $db;
    $veri = $db->prepare("SELECT users_up_sml_picture FROM users WHERE users_id='$id'");
    $veri->execute(array());
    $v = $veri->fetchAll(PDO::FETCH_ASSOC);
    foreach ($v as $kapak){
    return $kapak['users_up_sml_picture'];
}
}

function kapakYukle($postisim, $yeniisim, $yol)
{
    // VEROT RESİM YÜKLEME
    $foo = new Upload($_FILES[$postisim]);
    if ($foo->uploaded) {
        $foo->allowed = array('image/*');
        $foo->file_new_name_body = $yeniisim;
        $foo->image_resize = true;
        $foo->image_ratio_crop  = true;
        $foo->image_x = 1920;
        $foo->image_y = 640;
        $foo->Process($yol);
        $foo->file_new_name_body = $yeniisim;
        $foo->image_resize = true;
        $foo->image_ratio_crop  = true;
        $foo->image_x = 318;
        $foo->image_y = 122;
        $foo->Process('../../img/users/uppicture/sml/');
        if ($foo->processed) {
            $foo->Clean();
            return true;
        } else {
            return false;
        }




    }


    // VEROT RESİM YÜKLEME
}

function kucukKapakYukle($postisim, $yeniisim, $yol)
{
    // VEROT RESİM YÜKLEME
    $foo = new Upload($_FILES[$postisim]);
    if ($foo->uploaded) {
        // save uploaded image with no changes
        $foo->Process('/home/user/files/');
        if ($foo->processed) {
          echo 'original image copied';
        } else {
          echo 'error : ' . $foo->error;
        }
        // save uploaded image with a new name
        $foo->file_new_name_body = 'foo';
        $foo->Process('/home/user/files/');
        if ($foo->processed) {
          echo 'image renamed "foo" copied';
        } else {
          echo 'error : ' . $foo->error;
        }   
        // save uploaded image with a new name,
        // resized to 100px wide
        $foo->file_new_name_body = 'image_resized';
        $foo->image_resize = true;
        $foo->image_convert = gif;
        $foo->image_x = 100;
        $foo->image_ratio_y = true;
        $foo->Process('/home/user/files/');
        if ($foo->processed) {
          echo 'image renamed, resized x=100
                and converted to GIF';
          $foo->Clean();
        } else {
          echo 'error : ' . $foo->error;
        } 
     }  


    // VEROT RESİM YÜKLEME
}


?>