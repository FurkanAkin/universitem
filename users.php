<?php
include_once 'inc/inc-head.php';


?>

<body>


<!-- Fixed Sidebar Left -->
<?php include_once 'modules/left-sidebar.php'; ?>
<!-- ... end Fixed Sidebar Left -->


<!-- Fixed Sidebar Right -->
<?php include_once 'modules/right-sidebar.php'; ?>
<!-- ... end Fixed Sidebar Right-Responsive -->

<!-- Header-BP -->
<?php include_once 'modules/header-modules.php'; ?>
<!-- ... end Responsive Header-BP -->
<!-- Friends -->
<div class="main-header">
<div class="container userslist">
    <div class="row">
        <?php

        $Sayfa = @intval($_GET['sayfa']);
        if (!$Sayfa) $Sayfa = 1;
        $Say = $db->query("select * from users order by users_id DESC");
        $ToplamVeri = $Say->rowCount();
        $Limit = 16;
        $Sayfa_Sayisi = ceil($ToplamVeri / $Limit);
        if ($Sayfa > $Sayfa_Sayisi) {
            $Sayfa = 1;
        }
        $Goster = $Sayfa * $Limit - $Limit;
        $GorunenSayfa = 5;


        $Kullanici = $db->query("select * from users order by users_id DESC limit $Goster,$Limit");

        $KullaniciAl = $Kullanici->fetchAll(PDO::FETCH_ASSOC);
        ?>

        <?php foreach ($KullaniciAl as $KullaniciBas) { ?>
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="ui-block">

                    <!-- Friend Item -->

                    <div class="friend-item">
                        <div class="friend-header-thumb">
                            <img src="<?php echo $KullaniciBas["users_up_sml_picture"]; ?>" alt="<?php echo $KullaniciBas["users_name"]." ".$KullaniciBas["users_surname"]; ?>">
                        </div>

                        <div class="friend-item-content">

                            <div class="more">
                                <svg class="olymp-three-dots-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                </svg>
                                <ul class="more-dropdown">
                                    <li>
                                        <a href="#">Report Profile</a>
                                    </li>
                                    <li>
                                        <a href="#">Block Profile</a>
                                    </li>
                                    <li>
                                        <a href="#">Turn Off Notifications</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="friend-avatar">
                                <div class="author-thumb">
                                    <img class="userslistpp" src="<?php echo $KullaniciBas["users_picture"]; ?>" alt="<?php echo $KullaniciBas["users_name"]." ".$KullaniciBas["users_surname"]; ?>">
                                </div>
                                <div class="author-content">
                                    <a href="users-profile.php?users_id=<?php echo $KullaniciBas["users_id"]; ?>" class="h5 author-name"><?php echo $KullaniciBas["users_name"]." ".$KullaniciBas["users_surname"]; ?></a>
                                    <div class="country"><?php echo $KullaniciBas["users_school"]; ?></div>
                                    <div class="country"><?php echo $KullaniciBas["users_city"]; ?></div>
                                </div>
                            </div>

                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="friend-count" data-swiper-parallax="-500">
                                            <a href="#" class="friend-count-item">
                                                <div class="h6">52</div>
                                                <div class="title">Arkadaş</div>
                                            </a>
                                            <a href="#" class="friend-count-item">
                                                <div class="h6">240</div>
                                                <div class="title">İçerik</div>
                                            </a>
                                            <a href="#" class="friend-count-item">
                                                <div class="h6">16</div>
                                                <div class="title">Yorum</div>
                                            </a>
                                        </div>
                                        <div class="control-block-button" data-swiper-parallax="-100">
                                            <a href="#" class="btn btn-control bg-blue">
                                                <svg class="olymp-happy-face-icon">
                                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-happy-face-icon"></use>
                                                </svg>
                                            </a>

                                            <a href="#" class="btn btn-control bg-purple">
                                                <svg class="olymp-chat---messages-icon">
                                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-chat---messages-icon"></use>
                                                </svg>
                                            </a>

                                        </div>
                                    </div>

                                    <div class="swiper-slide">
                                        <p class="friend-about" data-swiper-parallax="-500">
                                            <?php echo $KullaniciBas["users_shortdesc"]; ?>
                                        </p>

                                        <div class="friend-since" data-swiper-parallax="-100">
                                            <span>Katılma Zamanı:</span>
                                            <div class="h6"><?php echo $KullaniciBas["users_created_date"]; ?></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- If we need pagination -->
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                    </div>

                    <!-- ... end Friend Item -->            </div>
            </div>
        <?php } ?>


        <nav aria-label="Page navigation">
            <ul class="pagination justify-content-center">
                <?php if ($Sayfa > 1) { ?>
                    <li class="page-item disabled">
                        <a class="page-link" href="peoples.php?sayfa=1" tabindex="-1">İlk</a>
                    </li>
                <?php } ?>
                <?php if ($Sayfa > 1) { ?>
                    <li class="page-item disabled">
                        <a class="page-link" href="peoples.php?sayfa=<?php echo $Sayfa - 1 ?>" tabindex="-1">Önceki</a>
                    </li>
                <?php } ?>
                <?php

                for ($i = $Sayfa - $GorunenSayfa; $i < $Sayfa + $GorunenSayfa + 1; $i++) {
                    if ($i > 0 and $i <= $Sayfa_Sayisi) {
                        if ($i == $Sayfa) {
                            echo '<li class="page-item disabled"><p class="page-link" href="#">' . $i . '</p></li>';
                        } else {
                            echo '<li class="page-item"><a class="page-link" href="peoples.php?sayfa=' . $i . '">' . $i . '</a></li>';
                        }
                    }
                }

                ?>

                <?php if ($Sayfa != $Sayfa_Sayisi) { ?>
                    <li class="page-item">
                        <a class="page-link" href="peoples.php?sayfa=<?php echo $Sayfa + 1 ?>">Sonraki</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="peoples.php?sayfa=<?php echo $Sayfa_Sayisi ?>">Son</a>
                    </li>
                <?php } ?>
            </ul>
        </nav>

    </div>
</div>
</div>

<!-- ... end Friends -->


<!-- Window-popup-CHAT for responsive min-width: 768px -->

<?php include_once 'modules/chat-popup.php'; ?>

<!-- ... end Window-popup-CHAT for responsive min-width: 768px -->

<a class="back-to-top" href="#">
    <img src="svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>
<!-- JS Scripts -->
<script src="js/jquery-3.2.1.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script src="js/perfect-scrollbar.js"></script>
<script src="js/jquery.matchHeight.js"></script>
<script src="js/svgxuse.js"></script>
<script src="js/imagesloaded.pkgd.js"></script>
<script src="js/Headroom.js"></script>
<script src="js/velocity.js"></script>
<script src="js/ScrollMagic.js"></script>
<script src="js/jquery.waypoints.js"></script>
<script src="js/jquery.countTo.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/material.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/smooth-scroll.js"></script>
<script src="js/selectize.js"></script>
<script src="js/swiper.jquery.js"></script>
<script src="js/moment.js"></script>
<script src="js/daterangepicker.js"></script>
<script src="js/simplecalendar.js"></script>
<script src="js/fullcalendar.js"></script>
<script src="js/isotope.pkgd.js"></script>
<script src="js/ajax-pagination.js"></script>
<script src="js/Chart.js"></script>
<script src="js/chartjs-plugin-deferred.js"></script>
<script src="js/circle-progress.js"></script>
<script src="js/loader.js"></script>
<script src="js/run-chart.js"></script>
<script src="js/jquery.magnific-popup.js"></script>
<script src="js/jquery.gifplayer.js"></script>
<script src="js/mediaelement-and-player.js"></script>
<script src="js/mediaelement-playlist-plugin.min.js"></script>

<script src="js/base-init.js"></script>

<script src="Bootstrap/dist/js/bootstrap.bundle.js"></script>

</body>
</html>