<?php
include_once 'inc/inc-head.php';
//KULLANICI GETİR
$kullanicisor=$db->prepare("SELECT * from users where users_id=:users_id");
$kullanicisor->execute(array(
    'users_id' => $_GET['users_id']
));

$kullanicicek=$kullanicisor->fetch(PDO::FETCH_ASSOC);
?>
<body>
<!-- Fixed Sidebar Left -->
<?php include_once 'modules/left-sidebar.php'; ?>
<!-- ... end Fixed Sidebar Left -->


<!-- Fixed Sidebar Left -->

<?php include_once 'modules/left-sidebar.php'; ?>

<!-- ... end Fixed Sidebar Left -->


<!-- Fixed Sidebar Right -->

<?php include_once 'modules/right-sidebar.php'; ?>

<!-- ... end Fixed Sidebar Right-Responsive -->


<!-- Header-BP -->
<?php include_once 'modules/header-modules.php'; ?>
<!-- ... end Responsive Header-BP -->
<div class="header-spacer"></div>


<!-- USERS-HEADER -->


<?php include_once 'modules/users/users-header.php'; ?>
<!-- ... end USERS-HEADER -->
<div class="container">
    <div class="row">

        <!-- Main Content -->

        <div class="col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-xs-12">
            <div id="newsfeed-items-grid">

                <div class="ui-block">
                    <!-- Post -->

                    <article class="hentry post">

                        <div class="post__author author vcard inline-items">
                            <img src="img/author-page.jpg" alt="author">

                            <div class="author-date">
                                <a class="h6 post__author-name fn" href="02-ProfilePage.html">Furkan Akın</a>
                                <div class="post__date">
                                    <time class="published" datetime="2017-03-24T18:18">
                                        19 saat önce
                                    </time>
                                </div>
                            </div>

                            <div class="more">
                                <svg class="olymp-three-dots-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                </svg>
                                <ul class="more-dropdown">
                                    <li>
                                        <a href="#">Edit Post</a>
                                    </li>
                                    <li>
                                        <a href="#">Delete Post</a>
                                    </li>
                                    <li>
                                        <a href="#">Turn Off Notifications</a>
                                    </li>
                                    <li>
                                        <a href="#">Select as Featured</a>
                                    </li>
                                </ul>
                            </div>

                        </div>

                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                            mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                            accusantium doloremque.
                        </p>

                        <div class="post-additional-info inline-items">

                            <a href="#" class="post-add-icon inline-items">
                                <svg class="olymp-heart-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-heart-icon"></use>
                                </svg>
                                <span>8</span>
                            </a>

                            <ul class="friends-harmonic">
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic7.jpg" alt="friend">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic8.jpg" alt="friend">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic9.jpg" alt="friend">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic10.jpg" alt="friend">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic11.jpg" alt="friend">
                                    </a>
                                </li>
                            </ul>

                            <div class="names-people-likes">
                                <a href="#">Jenny</a>, <a href="#">Robert</a> and
                                <br>6 more liked this
                            </div>


                            <div class="comments-shared">
                                <a href="#" class="post-add-icon inline-items">
                                    <svg class="olymp-speech-balloon-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use>
                                    </svg>
                                    <span>17</span>
                                </a>

                                <a href="#" class="post-add-icon inline-items">
                                    <svg class="olymp-share-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-share-icon"></use>
                                    </svg>
                                    <span>24</span>
                                </a>
                            </div>


                        </div>

                        <div class="control-block-button post-control-button">

                            <a href="#" class="btn btn-control featured-post">
                                <svg class="olymp-trophy-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-trophy-icon"></use>
                                </svg>
                            </a>

                            <a href="#" class="btn btn-control">
                                <svg class="olymp-like-post-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-like-post-icon"></use>
                                </svg>
                            </a>

                            <a href="#" class="btn btn-control">
                                <svg class="olymp-comments-post-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-comments-post-icon"></use>
                                </svg>
                            </a>

                            <a href="#" class="btn btn-control">
                                <svg class="olymp-share-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-share-icon"></use>
                                </svg>
                            </a>

                        </div>

                    </article>

                    <!-- .. end Post -->                </div>
                <div class="ui-block">

                    <!-- Post -->

                    <article class="hentry post video">

                        <div class="post__author author vcard inline-items">
                            <img src="img/author-page.jpg" alt="author">

                            <div class="author-date">
                                <a class="h6 post__author-name fn" href="02-ProfilePage.html">Furkan Akın</a> shared a
                                <a href="#">link</a>
                                <div class="post__date">
                                    <time class="published" datetime="2017-03-24T18:18">
                                        7 saat önce
                                    </time>
                                </div>
                            </div>

                            <div class="more">
                                <svg class="olymp-three-dots-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                </svg>
                                <ul class="more-dropdown">
                                    <li>
                                        <a href="#">Edit Post</a>
                                    </li>
                                    <li>
                                        <a href="#">Delete Post</a>
                                    </li>
                                    <li>
                                        <a href="#">Turn Off Notifications</a>
                                    </li>
                                    <li>
                                        <a href="#">Select as Featured</a>
                                    </li>
                                </ul>
                            </div>

                        </div>

                        <p>If someone missed it, check out the new song by System of a Revenge! I thinks they are going
                            back to their roots...</p>

                        <div class="post-video">
                            <div class="video-thumb">
                                <img src="img/video5.jpg" alt="photo">
                                <a href="https://youtube.com/watch?v=excVFQ2TWig" class="play-video">
                                    <svg class="olymp-play-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-play-icon"></use>
                                    </svg>
                                </a>
                            </div>

                            <div class="video-content">
                                <a href="#" class="h4 title">System of a Revenge - Nothing Else Matters (LIVE)</a>
                                <p>Lorem ipsum dolor sit amet, consectetur ipisicing elit, sed do eiusmod tempo
                                    incididunt ut labore..</p>
                                <a href="#" class="link-site">YOUTUBE.COM</a>
                            </div>
                        </div>

                        <div class="post-additional-info inline-items">

                            <a href="#" class="post-add-icon inline-items">
                                <svg class="olymp-heart-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-heart-icon"></use>
                                </svg>
                                <span>15</span>
                            </a>

                            <ul class="friends-harmonic">
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic9.jpg" alt="friend">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic10.jpg" alt="friend">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic7.jpg" alt="friend">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic8.jpg" alt="friend">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic11.jpg" alt="friend">
                                    </a>
                                </li>
                            </ul>

                            <div class="names-people-likes">
                                <a href="#">Jenny</a>, <a href="#">Robert</a> and
                                <br>13 more liked this
                            </div>

                            <div class="comments-shared">
                                <a href="#" class="post-add-icon inline-items">
                                    <svg class="olymp-speech-balloon-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use>
                                    </svg>
                                    <span>1</span>
                                </a>

                                <a href="#" class="post-add-icon inline-items">
                                    <svg class="olymp-share-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-share-icon"></use>
                                    </svg>
                                    <span>16</span>
                                </a>
                            </div>


                        </div>

                        <div class="control-block-button post-control-button">

                            <a href="#" class="btn btn-control">
                                <svg class="olymp-like-post-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-like-post-icon"></use>
                                </svg>
                            </a>

                            <a href="#" class="btn btn-control">
                                <svg class="olymp-comments-post-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-comments-post-icon"></use>
                                </svg>
                            </a>

                            <a href="#" class="btn btn-control">
                                <svg class="olymp-share-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-share-icon"></use>
                                </svg>
                            </a>

                        </div>

                    </article>

                    <!-- .. end Post -->                </div>
                <div class="ui-block">
                    <!-- Post -->

                    <article class="hentry post">

                        <div class="post__author author vcard inline-items">
                            <img src="img/author-page.jpg" alt="author">

                            <div class="author-date">
                                <a class="h6 post__author-name fn" href="02-ProfilePage.html">Furkan Akın</a>
                                <div class="post__date">
                                    <time class="published" datetime="2017-03-24T18:18">
                                        2 saat önce
                                    </time>
                                </div>
                            </div>

                            <div class="more">
                                <svg class="olymp-three-dots-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                </svg>
                                <ul class="more-dropdown">
                                    <li>
                                        <a href="#">Edit Post</a>
                                    </li>
                                    <li>
                                        <a href="#">Delete Post</a>
                                    </li>
                                    <li>
                                        <a href="#">Turn Off Notifications</a>
                                    </li>
                                    <li>
                                        <a href="#">Select as Featured</a>
                                    </li>
                                </ul>
                            </div>

                        </div>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempo incididunt ut
                            labore et
                            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                            consequat.
                        </p>

                        <div class="post-additional-info inline-items">

                            <a href="#" class="post-add-icon inline-items">
                                <svg class="olymp-heart-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-heart-icon"></use>
                                </svg>
                                <span>36</span>
                            </a>

                            <ul class="friends-harmonic">
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic7.jpg" alt="friend">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic8.jpg" alt="friend">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic9.jpg" alt="friend">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic10.jpg" alt="friend">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic11.jpg" alt="friend">
                                    </a>
                                </li>
                            </ul>

                            <div class="names-people-likes">
                                <a href="#">You</a>, <a href="#">Elaine</a> and
                                <br>34 more liked this
                            </div>


                            <div class="comments-shared">
                                <a href="#" class="post-add-icon inline-items">
                                    <svg class="olymp-speech-balloon-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use>
                                    </svg>
                                    <span>17</span>
                                </a>

                                <a href="#" class="post-add-icon inline-items">
                                    <svg class="olymp-share-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-share-icon"></use>
                                    </svg>
                                    <span>24</span>
                                </a>
                            </div>


                        </div>

                        <div class="control-block-button post-control-button">

                            <a href="#" class="btn btn-control">
                                <svg class="olymp-like-post-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-like-post-icon"></use>
                                </svg>
                            </a>

                            <a href="#" class="btn btn-control">
                                <svg class="olymp-comments-post-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-comments-post-icon"></use>
                                </svg>
                            </a>

                            <a href="#" class="btn btn-control">
                                <svg class="olymp-share-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-share-icon"></use>
                                </svg>
                            </a>

                        </div>

                    </article>

                    <!-- .. end Post -->
                    <!-- Comments -->

                    <ul class="comments-list">
                        <li>
                            <div class="post__author author vcard inline-items">
                                <img src="img/avatar10-sm.jpg" alt="author">

                                <div class="author-date">
                                    <a class="h6 post__author-name fn" href="#">Elaine Dreyfuss</a>
                                    <div class="post__date">
                                        <time class="published" datetime="2017-03-24T18:18">
                                            5 mins ago
                                        </time>
                                    </div>
                                </div>

                                <a href="#" class="more">
                                    <svg class="olymp-three-dots-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                    </svg>
                                </a>

                            </div>

                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium der doloremque
                                laudantium.</p>

                            <a href="#" class="post-add-icon inline-items">
                                <svg class="olymp-heart-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-heart-icon"></use>
                                </svg>
                                <span>8</span>
                            </a>
                            <a href="#" class="reply">Reply</a>
                        </li>
                        <li class="has-children">
                            <div class="post__author author vcard inline-items">
                                <img src="img/avatar5-sm.jpg" alt="author">

                                <div class="author-date">
                                    <a class="h6 post__author-name fn" href="#">Green Goo Rock</a>
                                    <div class="post__date">
                                        <time class="published" datetime="2017-03-24T18:18">
                                            1 hour ago
                                        </time>
                                    </div>
                                </div>

                                <a href="#" class="more">
                                    <svg class="olymp-three-dots-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                    </svg>
                                </a>

                            </div>

                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugiten, sed quia
                                consequuntur magni dolores eos qui ratione voluptatem sequi en lod nesciunt. Neque porro
                                quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur adipisci velit en lorem
                                ipsum der.
                            </p>

                            <a href="#" class="post-add-icon inline-items">
                                <svg class="olymp-heart-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-heart-icon"></use>
                                </svg>
                                <span>5</span>
                            </a>
                            <a href="#" class="reply">Reply</a>

                            <ul class="children">
                                <li>
                                    <div class="post__author author vcard inline-items">
                                        <img src="img/avatar8-sm.jpg" alt="author">

                                        <div class="author-date">
                                            <a class="h6 post__author-name fn" href="#">Diana Jameson</a>
                                            <div class="post__date">
                                                <time class="published" datetime="2017-03-24T18:18">
                                                    39 mins ago
                                                </time>
                                            </div>
                                        </div>

                                        <a href="#" class="more">
                                            <svg class="olymp-three-dots-icon">
                                                <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                            </svg>
                                        </a>

                                    </div>

                                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur.</p>

                                    <a href="#" class="post-add-icon inline-items">
                                        <svg class="olymp-heart-icon">
                                            <use xlink:href="svg-icons/sprites/icons.svg#olymp-heart-icon"></use>
                                        </svg>
                                        <span>2</span>
                                    </a>
                                    <a href="#" class="reply">Reply</a>
                                </li>
                                <li>
                                    <div class="post__author author vcard inline-items">
                                        <img src="img/avatar2-sm.jpg" alt="author">

                                        <div class="author-date">
                                            <a class="h6 post__author-name fn" href="#">Nicholas Grisom</a>
                                            <div class="post__date">
                                                <time class="published" datetime="2017-03-24T18:18">
                                                    24 mins ago
                                                </time>
                                            </div>
                                        </div>

                                        <a href="#" class="more">
                                            <svg class="olymp-three-dots-icon">
                                                <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                            </svg>
                                        </a>

                                    </div>

                                    <p>Excepteur sint occaecat cupidatat non proident.</p>

                                    <a href="#" class="post-add-icon inline-items">
                                        <svg class="olymp-heart-icon">
                                            <use xlink:href="svg-icons/sprites/icons.svg#olymp-heart-icon"></use>
                                        </svg>
                                        <span>0</span>
                                    </a>
                                    <a href="#" class="reply">Reply</a>

                                </li>
                            </ul>

                        </li>

                        <li>
                            <div class="post__author author vcard inline-items">
                                <img src="img/avatar4-sm.jpg" alt="author">

                                <div class="author-date">
                                    <a class="h6 post__author-name fn" href="#">Chris Greyson</a>
                                    <div class="post__date">
                                        <time class="published" datetime="2017-03-24T18:18">
                                            1 hour ago
                                        </time>
                                    </div>
                                </div>

                                <a href="#" class="more">
                                    <svg class="olymp-three-dots-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                    </svg>
                                </a>

                            </div>

                            <p>Dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                culpa qui officia deserunt mollit.</p>

                            <a href="#" class="post-add-icon inline-items">
                                <svg class="olymp-heart-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-heart-icon"></use>
                                </svg>
                                <span>7</span>
                            </a>
                            <a href="#" class="reply">Reply</a>

                        </li>
                    </ul>

                    <!-- ... end Comments -->
                    <a href="#" class="more-comments">View more comments <span>+</span></a>

                    <!-- Comment Form  -->

                    <form class="comment-form inline-items">

                        <div class="post__author author vcard inline-items">
                            <img src="img/author-page.jpg" alt="author">

                            <div class="form-group with-icon-right ">
                                <textarea class="form-control" placeholder=""></textarea>
                                <div class="add-options-message">
                                    <a href="#" class="options-message" data-toggle="modal"
                                       data-target="#update-header-photo">
                                        <svg class="olymp-camera-icon">
                                            <use xlink:href="svg-icons/sprites/icons.svg#olymp-camera-icon"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-md-2 btn-primary">Post Comment</button>

                        <button class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color">Cancel
                        </button>

                    </form>

                    <!-- ... end Comment Form  -->                </div>
                <div class="ui-block">
                    <!-- Post -->

                    <article class="hentry post has-post-thumbnail shared-photo">

                        <div class="post__author author vcard inline-items">
                            <img src="img/author-page.jpg" alt="author">

                            <div class="author-date">
                                <a class="h6 post__author-name fn" href="02-ProfilePage.html">Furkan Akın</a> shared
                                <a href="#">Diana Jameson</a>’s <a href="#">photo</a>
                                <div class="post__date">
                                    <time class="published" datetime="2017-03-24T18:18">
                                        7 saat önce
                                    </time>
                                </div>
                            </div>

                            <div class="more">
                                <svg class="olymp-three-dots-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                </svg>
                                <ul class="more-dropdown">
                                    <li>
                                        <a href="#">Edit Post</a>
                                    </li>
                                    <li>
                                        <a href="#">Delete Post</a>
                                    </li>
                                    <li>
                                        <a href="#">Turn Off Notifications</a>
                                    </li>
                                    <li>
                                        <a href="#">Select as Featured</a>
                                    </li>
                                </ul>
                            </div>

                        </div>

                        <p>Hi! Everyone should check out these amazing photographs that my friend shot the past week.
                            Here’s one of them...leave a kind comment!</p>

                        <div class="post-thumb">
                            <img src="img/post-photo6.jpg" alt="photo">
                        </div>

                        <ul class="children single-children">
                            <li>
                                <div class="post__author author vcard inline-items">
                                    <img src="img/avatar8-sm.jpg" alt="author">
                                    <div class="author-date">
                                        <a class="h6 post__author-name fn" href="#">Diana Jameson</a>
                                        <div class="post__date">
                                            <time class="published" datetime="2017-03-24T18:18">
                                                16 saat önce
                                            </time>
                                        </div>
                                    </div>
                                </div>

                                <p>Here’s the first photo of our incredible photoshoot from yesterday. If you like it
                                    please say so and tel me what you wanna see next!</p>
                            </li>
                        </ul>

                        <div class="post-additional-info inline-items">

                            <a href="#" class="post-add-icon inline-items">
                                <svg class="olymp-heart-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-heart-icon"></use>
                                </svg>
                                <span>15</span>
                            </a>

                            <ul class="friends-harmonic">
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic5.jpg" alt="friend">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic10.jpg" alt="friend">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic7.jpg" alt="friend">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic8.jpg" alt="friend">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="img/friend-harmonic2.jpg" alt="friend">
                                    </a>
                                </li>
                            </ul>

                            <div class="names-people-likes">
                                <a href="#">Diana</a>, <a href="#">Nicholas</a> and
                                <br>13 more liked this
                            </div>

                            <div class="comments-shared">
                                <a href="#" class="post-add-icon inline-items">
                                    <svg class="olymp-speech-balloon-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use>
                                    </svg>
                                    <span>0</span>
                                </a>

                                <a href="#" class="post-add-icon inline-items">
                                    <svg class="olymp-share-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-share-icon"></use>
                                    </svg>
                                    <span>16</span>
                                </a>
                            </div>

                        </div>

                        <div class="control-block-button post-control-button">

                            <a href="#" class="btn btn-control">
                                <svg class="olymp-like-post-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-like-post-icon"></use>
                                </svg>
                            </a>

                            <a href="#" class="btn btn-control">
                                <svg class="olymp-comments-post-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-comments-post-icon"></use>
                                </svg>
                            </a>

                            <a href="#" class="btn btn-control">
                                <svg class="olymp-share-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-share-icon"></use>
                                </svg>
                            </a>

                        </div>

                    </article>

                    <!-- .. end Post -->                </div>
            </div>

            <a id="load-more-button" href="#" class="btn btn-control btn-more" data-load-link="items-to-load.html"
               data-container="newsfeed-items-grid">
                <svg class="olymp-three-dots-icon">
                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                </svg>
            </a>
        </div>

        <!-- ... end Main Content -->


        <!-- Left Sidebar -->

        <div class="col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-xs-12">
            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">Hakkımda</h6>
                </div>
                <div class="ui-block-content">

                    <!-- W-Personal-Info -->

                    <ul class="widget w-personal-info item-block">
                        <li>
                            <span class="title">Kısaca ben:</span>
                            <span class="text"><?php echo $kullanicicek['users_shortdesc']; ?></span>
                        </li>
                        <li>
                            <span class="title">Favori Dizilerim:</span>
                            <span class="text"><?php echo $kullanicicek['users_fav_series']; ?></span>
                        </li>
                        <li>
                            <span class="title">Favori Kitaplarım:</span>
                            <span class="text"><?php echo $kullanicicek['users_fav_books']; ?></span>
                        </li>
                    </ul>

                    <!-- .. end W-Personal-Info -->
                    <!-- W-Socials -->

                    <div class="widget w-socials">
                        <h6 class="title">Sosyal Medya Hesaplarım:</h6>

                        <!-- SOSYAL MEDYA HESAPLARI VARSA TEK TEK EKLENİYOR EĞER İÇİ BOŞSA EKLENMİYOR -->
                        <?php
                        if ($kullanicicek['users_facebook']) {
                            ?>
                            <a href="<?php echo $kullanicicek['users_facebook']; ?>" class="social-item bg-facebook">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                Facebook
                            </a>
                        <?php } ?>
                        <?php
                        if ($kullanicicek['users_twitter']) {
                            ?>
                            <a href="<?php echo $kullanicicek['users_twitter']; ?>" class="social-item bg-twitter">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                Twitter
                            </a>
                        <?php } ?>
                        <?php
                        if ($kullanicicek['users_instagram']) {
                            ?>
                            <a href="<?php echo $kullanicicek['users_instagram']; ?>" class="social-item btn-purple">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                                Instagram
                            </a>
                        <?php } ?>
                        <?php
                        if ($kullanicicek['users_youtube']) {
                            ?>
                            <a href="<?php echo $kullanicicek['users_youtube']; ?>" class="social-item bg-google">
                                <i class="fa fa-youtube" aria-hidden="true"></i>
                                YouTube
                            </a>
                        <?php } ?>
                        <?php
                        if ($kullanicicek['users_website']) {
                            ?>
                            <a href="<?php echo $kullanicicek['users_website']; ?>" class="social-item btn-grey">
                                <i class="fa fa-globe" aria-hidden="true"></i>
                                Website
                            </a>
                        <?php } ?>
                    </div>


                    <!-- ... end W-Socials -->
                </div>
            </div>

            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">James’s Badges</h6>
                </div>
                <div class="ui-block-content">

                    <!-- W-Badges -->

                    <ul class="widget w-badges">
                        <li>
                            <a href="24-CommunityBadges.html">
                                <img src="img/badge1.png" alt="author">
                                <div class="label-avatar bg-primary">2</div>
                            </a>
                        </li>
                        <li>
                            <a href="24-CommunityBadges.html">
                                <img src="img/badge4.png" alt="author">
                            </a>
                        </li>
                        <li>
                            <a href="24-CommunityBadges.html">
                                <img src="img/badge3.png" alt="author">
                                <div class="label-avatar bg-blue">4</div>
                            </a>
                        </li>
                        <li>
                            <a href="24-CommunityBadges.html">
                                <img src="img/badge6.png" alt="author">
                            </a>
                        </li>
                        <li>
                            <a href="24-CommunityBadges.html">
                                <img src="img/badge11.png" alt="author">
                            </a>
                        </li>
                        <li>
                            <a href="24-CommunityBadges.html">
                                <img src="img/badge8.png" alt="author">
                            </a>
                        </li>
                        <li>
                            <a href="24-CommunityBadges.html">
                                <img src="img/badge10.png" alt="author">
                            </a>
                        </li>
                        <li>
                            <a href="24-CommunityBadges.html">
                                <img src="img/badge13.png" alt="author">
                                <div class="label-avatar bg-breez">2</div>
                            </a>
                        </li>
                        <li>
                            <a href="24-CommunityBadges.html">
                                <img src="img/badge7.png" alt="author">
                            </a>
                        </li>
                        <li>
                            <a href="24-CommunityBadges.html">
                                <img src="img/badge12.png" alt="author">
                            </a>
                        </li>
                    </ul>

                    <!--.. end W-Badges -->
                </div>
            </div>

            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">My Spotify Playlist</h6>
                </div>

                <!-- W-Playlist -->

                <ol class="widget w-playlist">
                    <li class="js-open-popup" data-popup-target=".playlist-popup">
                        <div class="playlist-thumb">
                            <img src="img/playlist6.jpg" alt="thumb-composition">
                            <div class="overlay"></div>
                            <a href="#" class="play-icon">
                                <svg class="olymp-music-play-icon-big">
                                    <use xlink:href="svg-icons/sprites/icons-music.svg#olymp-music-play-icon-big"></use>
                                </svg>
                            </a>
                        </div>

                        <div class="composition">
                            <a href="#" class="composition-name">The Past Starts Slow...</a>
                            <a href="#" class="composition-author">System of a Revenge</a>
                        </div>

                        <div class="composition-time">
                            <time class="published" datetime="2017-03-24T18:18">3:22</time>
                            <div class="more">
                                <svg class="olymp-three-dots-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                </svg>
                                <ul class="more-dropdown">
                                    <li>
                                        <a href="#">Add Song to Player</a>
                                    </li>
                                    <li>
                                        <a href="#">Add Playlist to Player</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </li>

                    <li class="js-open-popup" data-popup-target=".playlist-popup">
                        <div class="playlist-thumb">
                            <img src="img/playlist7.jpg" alt="thumb-composition">
                            <div class="overlay"></div>
                            <a href="#" class="play-icon">
                                <svg class="olymp-music-play-icon-big">
                                    <use xlink:href="svg-icons/sprites/icons-music.svg#olymp-music-play-icon-big"></use>
                                </svg>
                            </a>
                        </div>

                        <div class="composition">
                            <a href="#" class="composition-name">The Pretender</a>
                            <a href="#" class="composition-author">Kung Fighters</a>
                        </div>

                        <div class="composition-time">
                            <time class="published" datetime="2017-03-24T18:18">5:48</time>
                            <div class="more">
                                <svg class="olymp-three-dots-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                </svg>
                                <ul class="more-dropdown">
                                    <li>
                                        <a href="#">Add Song to Player</a>
                                    </li>
                                    <li>
                                        <a href="#">Add Playlist to Player</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </li>
                    <li class="js-open-popup" data-popup-target=".playlist-popup">
                        <div class="playlist-thumb">
                            <img src="img/playlist8.jpg" alt="thumb-composition">
                            <div class="overlay"></div>
                            <a href="#" class="play-icon">
                                <svg class="olymp-music-play-icon-big">
                                    <use xlink:href="svg-icons/sprites/icons-music.svg#olymp-music-play-icon-big"></use>
                                </svg>
                            </a>
                        </div>

                        <div class="composition">
                            <a href="#" class="composition-name">Blood Brothers</a>
                            <a href="#" class="composition-author">Iron Maid</a>
                        </div>

                        <div class="composition-time">
                            <time class="published" datetime="2017-03-24T18:18">3:06</time>
                            <div class="more">
                                <svg class="olymp-three-dots-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                </svg>
                                <ul class="more-dropdown">
                                    <li>
                                        <a href="#">Add Song to Player</a>
                                    </li>
                                    <li>
                                        <a href="#">Add Playlist to Player</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </li>
                    <li class="js-open-popup" data-popup-target=".playlist-popup">
                        <div class="playlist-thumb">
                            <img src="img/playlist9.jpg" alt="thumb-composition">
                            <div class="overlay"></div>
                            <a href="#" class="play-icon">
                                <svg class="olymp-music-play-icon-big">
                                    <use xlink:href="svg-icons/sprites/icons-music.svg#olymp-music-play-icon-big"></use>
                                </svg>
                            </a>
                        </div>

                        <div class="composition">
                            <a href="#" class="composition-name">Seven Nation Army</a>
                            <a href="#" class="composition-author">The Black Stripes</a>
                        </div>

                        <div class="composition-time">
                            <time class="published" datetime="2017-03-24T18:18">6:17</time>
                            <div class="more">
                                <svg class="olymp-three-dots-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                </svg>
                                <ul class="more-dropdown">
                                    <li>
                                        <a href="#">Add Song to Player</a>
                                    </li>
                                    <li>
                                        <a href="#">Add Playlist to Player</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </li>
                    <li class="js-open-popup" data-popup-target=".playlist-popup">
                        <div class="playlist-thumb">
                            <img src="img/playlist10.jpg" alt="thumb-composition">
                            <div class="overlay"></div>
                            <a href="#" class="play-icon">
                                <svg class="olymp-music-play-icon-big">
                                    <use xlink:href="svg-icons/sprites/icons-music.svg#olymp-music-play-icon-big"></use>
                                </svg>
                            </a>
                        </div>

                        <div class="composition">
                            <a href="#" class="composition-name">Killer Queen</a>
                            <a href="#" class="composition-author">Archiduke</a>
                        </div>

                        <div class="composition-time">
                            <time class="published" datetime="2017-03-24T18:18">5:40</time>
                            <div class="more">
                                <svg class="olymp-three-dots-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                </svg>
                                <ul class="more-dropdown">
                                    <li>
                                        <a href="#">Add Song to Player</a>
                                    </li>
                                    <li>
                                        <a href="#">Add Playlist to Player</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </li>
                </ol>

                <!-- .. end W-Playlist -->
            </div>

            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">Twitter Gönderilerim</h6>
                </div>

                <!-- W-Twitter -->

                <ul class="widget w-twitter">
                    <li class="twitter-item">
                        <div class="author-folder">
                            <img src="img/twitter-avatar1.png" alt="avatar">
                            <div class="author">
                                <a href="#" class="author-name">Space Cowboy</a>
                                <a href="#" class="group">@james_spiegelOK</a>
                            </div>
                        </div>
                        <p>Tomorrow with the agency we will run 5 km for charity. Come and give us your support!
                            <a href="#" class="link-post">#Daydream5K</a></p>
                        <span class="post__date">
								<time class="published" datetime="2017-03-24T18:18">
									2 saat önce
								</time>
							</span>
                    </li>
                    <li class="twitter-item">
                        <div class="author-folder">
                            <img src="img/twitter-avatar1.png" alt="avatar">
                            <div class="author">
                                <a href="#" class="author-name">Space Cowboy</a>
                                <a href="#" class="group">@james_spiegelOK</a>
                            </div>
                        </div>
                        <p>Check out the new website of “The Bebop Bar”! <a href="#" class="link-post">bytle/thbp53f</a>
                        </p>
                        <span class="post__date">
								<time class="published" datetime="2017-03-24T18:18">
									16 saat önce
								</time>
							</span>
                    </li>
                    <li class="twitter-item">
                        <div class="author-folder">
                            <img src="img/twitter-avatar1.png" alt="avatar">
                            <div class="author">
                                <a href="#" class="author-name">Space Cowboy</a>
                                <a href="#" class="group">@james_spiegelOK</a>
                            </div>
                        </div>
                        <p>The Sunday is the annual agency camping trip and I still haven’t got a tent
                            <a href="#" class="link-post">#TheWild #Indoors</a></p>
                        <span class="post__date">
								<time class="published" datetime="2017-03-24T18:18">
									Yesterday
								</time>
							</span>
                    </li>
                </ul>


                <!-- .. end W-Twitter -->
            </div>

            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">Last Videos</h6>
                </div>
                <div class="ui-block-content">

                    <!-- W-Latest-Video -->

                    <ul class="widget w-last-video">
                        <li>
                            <a href="https://vimeo.com/ondemand/viewfromabluemoon4k/147865858"
                               class="play-video play-video--small">
                                <svg class="olymp-play-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-play-icon"></use>
                                </svg>
                            </a>
                            <img src="img/video8.jpg" alt="video">
                            <div class="video-content">
                                <div class="title">System of a Revenge - Hypnotize...</div>
                                <time class="published" datetime="2017-03-24T18:18">3:25</time>
                            </div>
                            <div class="overlay"></div>
                        </li>
                        <li>
                            <a href="https://youtube.com/watch?v=excVFQ2TWig" class="play-video play-video--small">
                                <svg class="olymp-play-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-play-icon"></use>
                                </svg>
                            </a>
                            <img src="img/video7.jpg" alt="video">
                            <div class="video-content">
                                <div class="title">Green Goo - Live at Dan’s Arena</div>
                                <time class="published" datetime="2017-03-24T18:18">5:48</time>
                            </div>
                            <div class="overlay"></div>
                        </li>
                    </ul>

                    <!-- .. end W-Latest-Video -->
                </div>
            </div>
        </div>

        <!-- ... end Left Sidebar -->


        <!-- Right Sidebar -->

        <div class="col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-xs-12">
            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">Son Fotoğraflar</h6>
                </div>
                <div class="ui-block-content">

                    <!-- W-Latest-Photo -->

                    <ul class="widget w-last-photo js-zoom-gallery">
                        <li>
                            <a href="img/last-photo10-large.jpg">
                                <img src="img/last-photo10-large.jpg" alt="photo">
                            </a>
                        </li>
                        <li>
                            <a href="img/last-photo10-large.jpg">
                                <img src="img/last-photo10-large.jpg" alt="photo">
                            </a>
                        </li>
                        <li>
                            <a href="img/last-photo10-large.jpg">
                                <img src="img/last-photo10-large.jpg" alt="photo">
                            </a>
                        </li>
                        <li>
                            <a href="img/last-photo10-large.jpg">
                                <img src="img/last-photo10-large.jpg" alt="photo">
                            </a>
                        </li>
                        <li>
                            <a href="img/last-photo10-large.jpg">
                                <img src="img/last-photo10-large.jpg" alt="photo">
                            </a>
                        </li>
                        <li>
                            <a href="img/last-photo10-large.jpg">
                                <img src="img/last-photo10-large.jpg" alt="photo">
                            </a>
                        </li>
                        <li>
                            <a href="img/last-photo10-large.jpg">
                                <img src="img/last-photo10-large.jpg" alt="photo">
                            </a>
                        </li>
                        <li>
                            <a href="img/last-photo10-large.jpg">
                                <img src="img/last-photo10-large.jpg" alt="photo">
                            </a>
                        </li>
                        <li>
                            <a href="img/last-photo10-large.jpg">
                                <img src="img/last-photo10-large.jpg" alt="photo">
                            </a>
                        </li>
                    </ul>


                    <!-- .. end W-Latest-Photo -->
                </div>
            </div>

            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">Blog</h6>
                </div>
                <!-- W-Blog-Posts -->

                <ul class="widget w-blog-posts">
                    <li>
                        <article class="hentry post">

                            <a href="#" class="h4">My Perfect Vacations in South America and Europe</a>

                            <p>Lorem ipsum dolor sit amet, consect adipisicing elit, sed do eiusmod por incidid ut
                                labore et.</p>

                            <div class="post__date">
                                <time class="published" datetime="2017-03-24T18:18">
                                    7 saat önce
                                </time>
                            </div>

                        </article>
                    </li>
                    <li>
                        <article class="hentry post">

                            <a href="#" class="h4">The Big Experience of Travelling Alone</a>

                            <p>Lorem ipsum dolor sit amet, consect adipisicing elit, sed do eiusmod por incidid ut
                                labore et.</p>

                            <div class="post__date">
                                <time class="published" datetime="2017-03-24T18:18">
                                    March 18th, at 6:52pm
                                </time>
                            </div>

                        </article>
                    </li>
                </ul>

                <!-- .. end W-Blog-Posts -->
            </div>

            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">Arkadaşlarım (86)</h6>
                </div>
                <div class="ui-block-content">

                    <!-- W-Faved-Page -->

                    <ul class="widget w-faved-page js-zoom-gallery">
                        <li>
                            <a href="#">
                                <img src="img/avatar38-sm.jpg" alt="author">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/avatar24-sm.jpg" alt="user">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/avatar36-sm.jpg" alt="author">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/avatar35-sm.jpg" alt="user">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/avatar34-sm.jpg" alt="author">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/avatar33-sm.jpg" alt="author">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/avatar32-sm.jpg" alt="user">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/avatar31-sm.jpg" alt="author">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/avatar30-sm.jpg" alt="author">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/avatar29-sm.jpg" alt="user">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/avatar28-sm.jpg" alt="user">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/avatar27-sm.jpg" alt="user">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/avatar26-sm.jpg" alt="user">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/avatar25-sm.jpg" alt="user">
                            </a>
                        </li>
                        <li class="all-users">
                            <a href="#">+74</a>
                        </li>
                    </ul>

                    <!-- .. end W-Faved-Page -->
                </div>
            </div>

            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">Favourite Pages</h6>
                </div>

                <!-- W-Friend-Pages-Added -->

                <ul class="widget w-friend-pages-added notification-list friend-requests">
                    <li class="inline-items">
                        <div class="author-thumb">
                            <img src="img/avatar41-sm.jpg" alt="author">
                        </div>
                        <div class="notification-event">
                            <a href="#" class="h6 notification-friend">The Marina Bar</a>
                            <span class="chat-message-item">Restaurant / Bar</span>
                        </div>
                        <span class="notification-icon" data-toggle="tooltip" data-placement="top"
                              data-original-title="ADD TO YOUR FAVS">
								<a href="#">
									<svg class="olymp-star-icon"><use
                                            xlink:href="svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>
								</a>
							</span>

                    </li>

                    <li class="inline-items">
                        <div class="author-thumb">
                            <img src="img/avatar42-sm.jpg" alt="author">
                        </div>
                        <div class="notification-event">
                            <a href="#" class="h6 notification-friend">Tapronus Rock</a>
                            <span class="chat-message-item">Rock Band</span>
                        </div>
                        <span class="notification-icon" data-toggle="tooltip" data-placement="top"
                              data-original-title="ADD TO YOUR FAVS">
								<a href="#">
									<svg class="olymp-star-icon"><use
                                            xlink:href="svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>
								</a>
							</span>

                    </li>

                    <li class="inline-items">
                        <div class="author-thumb">
                            <img src="img/avatar43-sm.jpg" alt="author">
                        </div>
                        <div class="notification-event">
                            <a href="#" class="h6 notification-friend">Pixel Digital Design</a>
                            <span class="chat-message-item">Company</span>
                        </div>
                        <span class="notification-icon" data-toggle="tooltip" data-placement="top"
                              data-original-title="ADD TO YOUR FAVS">
								<a href="#">
									<svg class="olymp-star-icon"><use
                                            xlink:href="svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>
								</a>
							</span>
                    </li>

                    <li class="inline-items">
                        <div class="author-thumb">
                            <img src="img/avatar44-sm.jpg" alt="author">
                        </div>
                        <div class="notification-event">
                            <a href="#" class="h6 notification-friend">Thompson’s Custom Clothing Boutique</a>
                            <span class="chat-message-item">Clothing Store</span>
                        </div>
                        <span class="notification-icon" data-toggle="tooltip" data-placement="top"
                              data-original-title="ADD TO YOUR FAVS">
								<a href="#">
									<svg class="olymp-star-icon"><use
                                            xlink:href="svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>
								</a>
							</span>

                    </li>

                    <li class="inline-items">
                        <div class="author-thumb">
                            <img src="img/avatar45-sm.jpg" alt="author">
                        </div>
                        <div class="notification-event">
                            <a href="#" class="h6 notification-friend">Crimson Agency</a>
                            <span class="chat-message-item">Company</span>
                        </div>
                        <span class="notification-icon" data-toggle="tooltip" data-placement="top"
                              data-original-title="ADD TO YOUR FAVS">
								<a href="#">
									<svg class="olymp-star-icon"><use
                                            xlink:href="svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>
								</a>
							</span>
                    </li>

                    <li class="inline-items">
                        <div class="author-thumb">
                            <img src="img/avatar46-sm.jpg" alt="author">
                        </div>
                        <div class="notification-event">
                            <a href="#" class="h6 notification-friend">Mannequin Angel</a>
                            <span class="chat-message-item">Clothing Store</span>
                        </div>
                        <span class="notification-icon" data-toggle="tooltip" data-placement="top"
                              data-original-title="ADD TO YOUR FAVS">
								<a href="#">
									<svg class="olymp-star-icon"><use
                                            xlink:href="svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>
								</a>
							</span>
                    </li>
                </ul>

                <!-- .. end W-Friend-Pages-Added -->
            </div>

            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">James's Poll</h6>
                </div>
                <div class="ui-block-content">

                    <!-- W-Pool -->

                    <ul class="widget w-pool">
                        <li>
                            <p>If you had to choose, which actor do you prefer to be the next Darkman? </p>
                        </li>
                        <li>
                            <div class="skills-item">
                                <div class="skills-item-info">
										<span class="skills-item-title">
											<span class="radio">
												<label>
													<input type="radio" name="optionsRadios">
													Thomas Bale
												</label>
											</span>
										</span>
                                    <span class="skills-item-count">
											<span class="count-animate" data-speed="1000" data-refresh-interval="50"
                                                  data-to="62" data-from="0"></span>
											<span class="units">62%</span>
										</span>
                                </div>
                                <div class="skills-item-meter">
                                    <span class="skills-item-meter-active bg-primary" style="width: 62%"></span>
                                </div>

                                <div class="counter-friends">12 friends voted for this</div>

                                <ul class="friends-harmonic">
                                    <li>
                                        <a href="#">
                                            <img src="img/friend-harmonic1.jpg" alt="friend">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/friend-harmonic2.jpg" alt="friend">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/friend-harmonic3.jpg" alt="friend">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/friend-harmonic4.jpg" alt="friend">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/friend-harmonic5.jpg" alt="friend">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/friend-harmonic6.jpg" alt="friend">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/friend-harmonic7.jpg" alt="friend">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/friend-harmonic8.jpg" alt="friend">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/friend-harmonic9.jpg" alt="friend">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="all-users">+3</a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li>
                            <div class="skills-item">
                                <div class="skills-item-info">
										<span class="skills-item-title">
											<span class="radio">
												<label>
													<input type="radio" name="optionsRadios">
													Ben Robertson
												</label>
											</span>
										</span>
                                    <span class="skills-item-count">
											<span class="count-animate" data-speed="1000" data-refresh-interval="50"
                                                  data-to="27" data-from="0"></span>
											<span class="units">27%</span>
										</span>
                                </div>
                                <div class="skills-item-meter">
                                    <span class="skills-item-meter-active bg-primary" style="width: 27%"></span>
                                </div>
                                <div class="counter-friends">7 friends voted for this</div>

                                <ul class="friends-harmonic">
                                    <li>
                                        <a href="#">
                                            <img src="img/friend-harmonic7.jpg" alt="friend">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/friend-harmonic8.jpg" alt="friend">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/friend-harmonic9.jpg" alt="friend">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/friend-harmonic10.jpg" alt="friend">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/friend-harmonic11.jpg" alt="friend">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/friend-harmonic12.jpg" alt="friend">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/friend-harmonic13.jpg" alt="friend">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li>
                            <div class="skills-item">
                                <div class="skills-item-info">
										<span class="skills-item-title">
											<span class="radio">
												<label>
													<input type="radio" name="optionsRadios">
													Michael Streiton
												</label>
											</span>
										</span>
                                    <span class="skills-item-count">
											<span class="count-animate" data-speed="1000" data-refresh-interval="50"
                                                  data-to="11" data-from="0"></span>
											<span class="units">11%</span>
										</span>
                                </div>
                                <div class="skills-item-meter">
                                    <span class="skills-item-meter-active bg-primary" style="width: 11%"></span>
                                </div>

                                <div class="counter-friends">2 people voted for this</div>

                                <ul class="friends-harmonic">
                                    <li>
                                        <a href="#">
                                            <img src="img/friend-harmonic14.jpg" alt="friend">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/friend-harmonic15.jpg" alt="friend">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>

                    <!-- .. end W-Pool -->
                    <a href="#" class="btn btn-md-2 btn-border-think custom-color c-grey full-width">Vote Now!</a>
                </div>
            </div>
        </div>

        <!-- ... end Right Sidebar -->

    </div>
</div>

<!-- Window-popup Update Header Photo -->

<div class="modal fade" id="update-header-photo">
    <div class="modal-dialog ui-block window-popup update-header-photo">
        <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
            <svg class="olymp-close-icon">
                <use xlink:href="svg-icons/sprites/icons.svg#olymp-close-icon"></use>
            </svg>
        </a>

        <div class="ui-block-title">
            <h6 class="title">Update Header Photo</h6>
        </div>

        <a href="#" class="upload-photo-item">
            <svg class="olymp-computer-icon">
                <use xlink:href="svg-icons/sprites/icons.svg#olymp-computer-icon"></use>
            </svg>

            <h6>Upload Photo</h6>
            <span>Browse your computer.</span>
        </a>

        <a href="#" class="upload-photo-item" data-toggle="modal" data-target="#choose-from-my-photo">

            <svg class="olymp-photos-icon">
                <use xlink:href="svg-icons/sprites/icons.svg#olymp-photos-icon"></use>
            </svg>

            <h6>Choose from My Photos</h6>
            <span>Choose from your uploaded photos</span>
        </a>
    </div>
</div>


<!-- ... end Window-popup Update Header Photo -->

<!-- Window-popup Choose from my Photo -->

<?php include_once 'modules/chat-popup.php'; ?>

<!-- ... end Window-popup Choose from my Photo -->

<!-- Playlist Popup -->

<div class="window-popup playlist-popup">

    <a href="" class="icon-close js-close-popup">
        <svg class="olymp-close-icon">
            <use xlink:href="svg-icons/sprites/icons.svg#olymp-close-icon"></use>
        </svg>
    </a>

    <table class="playlist-popup-table">

        <thead>

        <tr>

            <th class="play">
                PLAY
            </th>

            <th class="cover">
                COVER
            </th>

            <th class="song-artist">
                SONG AND ARTIST
            </th>

            <th class="album">
                ALBUM
            </th>

            <th class="released">
                RELEASED
            </th>

            <th class="duration">
                DURATION
            </th>

            <th class="spotify">
                GET IT ON SPOTIFY
            </th>

            <th class="remove">
                REMOVE
            </th>
        </tr>

        </thead>

        <tbody>
        <tr>
            <td class="play">
                <a href="#" class="play-icon">
                    <svg class="olymp-music-play-icon-big">
                        <use xlink:href="svg-icons/sprites/icons-music.svg#olymp-music-play-icon-big"></use>
                    </svg>
                </a>
            </td>
            <td class="cover">
                <div class="playlist-thumb">
                    <img src="img/playlist19.jpg" alt="thumb-composition">
                </div>
            </td>
            <td class="song-artist">
                <div class="composition">
                    <a href="#" class="composition-name">We Can Be Heroes</a>
                    <a href="#" class="composition-author">Jason Bowie</a>
                </div>
            </td>
            <td class="album">
                <a href="#" class="album-composition">Ziggy Firedust</a>
            </td>
            <td class="released">
                <div class="release-year">2014</div>
            </td>
            <td class="duration">
                <div class="composition-time">
                    <time class="published" datetime="2017-03-24T18:18">6:17</time>
                </div>
            </td>
            <td class="spotify">
                <i class="fa fa-spotify composition-icon" aria-hidden="true"></i>
            </td>
            <td class="remove">
                <a href="#" class="remove-icon">
                    <svg class="olymp-close-icon">
                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-close-icon"></use>
                    </svg>
                </a>
            </td>
        </tr>

        <tr>
            <td class="play">
                <a href="#" class="play-icon">
                    <svg class="olymp-music-play-icon-big">
                        <use xlink:href="svg-icons/sprites/icons-music.svg#olymp-music-play-icon-big"></use>
                    </svg>
                </a>
            </td>
            <td class="cover">
                <div class="playlist-thumb">
                    <img src="img/playlist6.jpg" alt="thumb-composition">
                </div>
            </td>
            <td class="song-artist">
                <div class="composition">
                    <a href="#" class="composition-name">The Past Starts Slow and Ends</a>
                    <a href="#" class="composition-author">System of a Revenge</a>
                </div>
            </td>
            <td class="album">
                <a href="#" class="album-composition">Wonderize</a>
            </td>
            <td class="released">
                <div class="release-year">2014</div>
            </td>
            <td class="duration">
                <div class="composition-time">
                    <time class="published" datetime="2017-03-24T18:18">6:17</time>
                </div>
            </td>
            <td class="spotify">
                <i class="fa fa-spotify composition-icon" aria-hidden="true"></i>
            </td>
            <td class="remove">
                <a href="#" class="remove-icon">
                    <svg class="olymp-close-icon">
                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-close-icon"></use>
                    </svg>
                </a>
            </td>
        </tr>

        <tr>
            <td class="play">
                <a href="#" class="play-icon">
                    <svg class="olymp-music-play-icon-big">
                        <use xlink:href="svg-icons/sprites/icons-music.svg#olymp-music-play-icon-big"></use>
                    </svg>
                </a>
            </td>
            <td class="cover">
                <div class="playlist-thumb">
                    <img src="img/playlist7.jpg" alt="thumb-composition">
                </div>
            </td>
            <td class="song-artist">
                <div class="composition">
                    <a href="#" class="composition-name">The Pretender</a>
                    <a href="#" class="composition-author">Kung Fighters</a>
                </div>
            </td>
            <td class="album">
                <a href="#" class="album-composition">Warping Lights</a>
            </td>
            <td class="released">
                <div class="release-year">2014</div>
            </td>
            <td class="duration">
                <div class="composition-time">
                    <time class="published" datetime="2017-03-24T18:18">6:17</time>
                </div>
            </td>
            <td class="spotify">
                <i class="fa fa-spotify composition-icon" aria-hidden="true"></i>
            </td>
            <td class="remove">
                <a href="#" class="remove-icon">
                    <svg class="olymp-close-icon">
                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-close-icon"></use>
                    </svg>
                </a>
            </td>
        </tr>

        <tr>
            <td class="play">
                <a href="#" class="play-icon">
                    <svg class="olymp-music-play-icon-big">
                        <use xlink:href="svg-icons/sprites/icons-music.svg#olymp-music-play-icon-big"></use>
                    </svg>
                </a>
            </td>
            <td class="cover">
                <div class="playlist-thumb">
                    <img src="img/playlist8.jpg" alt="thumb-composition">
                </div>
            </td>
            <td class="song-artist">
                <div class="composition">
                    <a href="#" class="composition-name">Seven Nation Army</a>
                    <a href="#" class="composition-author">The Black Stripes</a>
                </div>
            </td>
            <td class="album">
                <a href="#" class="album-composition ">Icky Strung (LIVE at Cube Garden)</a>
            </td>
            <td class="released">
                <div class="release-year">2014</div>
            </td>
            <td class="duration">
                <div class="composition-time">
                    <time class="published" datetime="2017-03-24T18:18">6:17</time>
                </div>
            </td>
            <td class="spotify">
                <i class="fa fa-spotify composition-icon" aria-hidden="true"></i>
            </td>
            <td class="remove">
                <a href="#" class="remove-icon">
                    <svg class="olymp-close-icon">
                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-close-icon"></use>
                    </svg>
                </a>
            </td>
        </tr>

        <tr>
            <td class="play">
                <a href="#" class="play-icon">
                    <svg class="olymp-music-play-icon-big">
                        <use xlink:href="svg-icons/sprites/icons-music.svg#olymp-music-play-icon-big"></use>
                    </svg>
                </a>
            </td>
            <td class="cover">
                <div class="playlist-thumb">
                    <img src="img/playlist9.jpg" alt="thumb-composition">
                </div>
            </td>
            <td class="song-artist">
                <div class="composition">
                    <a href="#" class="composition-name">Leap of Faith</a>
                    <a href="#" class="composition-author">Eden Artifact</a>
                </div>
            </td>
            <td class="album">
                <a href="#" class="album-composition">The Assassins’s Soundtrack</a>
            </td>
            <td class="released">
                <div class="release-year">2014</div>
            </td>
            <td class="duration">
                <div class="composition-time">
                    <time class="published" datetime="2017-03-24T18:18">6:17</time>
                </div>
            </td>
            <td class="spotify">
                <i class="fa fa-spotify composition-icon" aria-hidden="true"></i>
            </td>
            <td class="remove">
                <a href="#" class="remove-icon">
                    <svg class="olymp-close-icon">
                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-close-icon"></use>
                    </svg>
                </a>
            </td>
        </tr>

        <tr>
            <td class="play">
                <a href="#" class="play-icon">
                    <svg class="olymp-music-play-icon-big">
                        <use xlink:href="svg-icons/sprites/icons-music.svg#olymp-music-play-icon-big"></use>
                    </svg>
                </a>
            </td>
            <td class="cover">
                <div class="playlist-thumb">
                    <img src="img/playlist10.jpg" alt="thumb-composition">
                </div>
            </td>
            <td class="song-artist">
                <div class="composition">
                    <a href="#" class="composition-name">Killer Queen</a>
                    <a href="#" class="composition-author">Archiduke</a>
                </div>
            </td>
            <td class="album">
                <a href="#" class="album-composition ">News of the Universe</a>
            </td>
            <td class="released">
                <div class="release-year">2014</div>
            </td>
            <td class="duration">
                <div class="composition-time">
                    <time class="published" datetime="2017-03-24T18:18">6:17</time>
                </div>
            </td>
            <td class="spotify">
                <i class="fa fa-spotify composition-icon" aria-hidden="true"></i>
            </td>
            <td class="remove">
                <a href="#" class="remove-icon">
                    <svg class="olymp-close-icon">
                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-close-icon"></use>
                    </svg>
                </a>
            </td>
        </tr>
        </tbody>
    </table>

    <audio id="mediaplayer" data-showplaylist="true">
        <source src="mp3/Twice.mp3" title="Track 1" data-poster="track1.png" type="audio/mpeg">
        <source src="mp3/Twice.mp3" title="Track 2" data-poster="track2.png" type="audio/mpeg">
        <source src="mp3/Twice.mp3" title="Track 3" data-poster="track3.png" type="audio/mpeg">
        <source src="mp3/Twice.mp3" title="Track 4" data-poster="track4.png" type="audio/mpeg">
    </audio>

</div>

<!-- ... end Playlist Popup -->


<a class="back-to-top" href="#">
    <img src="svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>


<!-- JS Scripts -->
<script src="js/jquery-3.2.1.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script src="js/perfect-scrollbar.js"></script>
<script src="js/jquery.matchHeight.js"></script>
<script src="js/svgxuse.js"></script>
<script src="js/imagesloaded.pkgd.js"></script>
<script src="js/Headroom.js"></script>
<script src="js/velocity.js"></script>
<script src="js/ScrollMagic.js"></script>
<script src="js/jquery.waypoints.js"></script>
<script src="js/jquery.countTo.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/material.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/smooth-scroll.js"></script>
<script src="js/selectize.js"></script>
<script src="js/swiper.jquery.js"></script>
<script src="js/moment.js"></script>
<script src="js/daterangepicker.js"></script>
<script src="js/simplecalendar.js"></script>
<script src="js/fullcalendar.js"></script>
<script src="js/isotope.pkgd.js"></script>
<script src="js/ajax-pagination.js"></script>
<script src="js/Chart.js"></script>
<script src="js/chartjs-plugin-deferred.js"></script>
<script src="js/circle-progress.js"></script>
<script src="js/loader.js"></script>
<script src="js/run-chart.js"></script>
<script src="js/jquery.magnific-popup.js"></script>
<script src="js/jquery.gifplayer.js"></script>
<script src="js/mediaelement-and-player.js"></script>
<script src="js/mediaelement-playlist-plugin.min.js"></script>

<script src="js/base-init.js"></script>

<script src="Bootstrap/dist/js/bootstrap.bundle.js"></script>


</body>
</html>