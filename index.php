<?php
	include_once 'boss/login/gpConfig.php';
	include_once 'boss/login/User.php';
	
	if(isset($_GET['code'])){
		$gClient->authenticate($_GET['code']);
		$_SESSION['token'] = $gClient->getAccessToken();
		header('Location:http://localhost/profile.php');
	}
	
	if (isset($_SESSION['token'])) {
		$gClient->setAccessToken($_SESSION['token']);
	}
	
	if ($gClient->getAccessToken()) {
		//Get user profile data from google
		$gpUserProfile = $google_oauthV2->userinfo->get();
		
		//Initialize User class
		$user = new User();
		
		//Insert or update user data to the database
		$gpUserData = array(
        'oauth_provider'=> 'google',
        'oauth_uid'     => $gpUserProfile['id'],
        'first_name'    => $gpUserProfile['given_name'],
        'last_name'     => $gpUserProfile['family_name'],
        'email'         => $gpUserProfile['email'],
        'gender'        => $gpUserProfile['gender'],
        'locale'        => $gpUserProfile['locale'],
        'picture'       => $gpUserProfile['picture'],
        'link'          => $gpUserProfile['link']
		);
		$userData = $user->checkUser($gpUserData);
		
		//Storing user data into session
		$_SESSION['userData'] = $userData;
		
		//Render facebook profile data
		if(!empty($userData)){
			$output = '<h1>Google+ Profile Details </h1>';
			$output .= '<img src="'.$userData['picture'].'" width="300" height="220">';
			$output .= '<br/>Google ID : ' . $userData['oauth_uid'];
			$output .= '<br/>Name : ' . $userData['first_name'].' '.$userData['last_name'];
			$output .= '<br/>Email : ' . $userData['email'];
			$output .= '<br/>Gender : ' . $userData['gender'];
			$output .= '<br/>Locale : ' . $userData['locale'];
			$output .= '<br/>Logged in with : Google';
			$output .= '<br/><a href="'.$userData['link'].'" target="_blank">Click to Visit Google+ Page</a>';
			$output .= '<br/>Logout from <a href="logout.php">Google</a>'; 
			}else{
			$output = '<h3 style="color:red">Some problem occurred, please try again.</h3>';
		}
		} else {
		$authUrl = $gClient->createAuthUrl();
		$output = '<a href="'.filter_var($authUrl, FILTER_SANITIZE_URL).'" class="btn btn-lg bg-google full-width btn-icon-left"><i class="fa fa-google" aria-hidden="true"></i>Google ile bağlan</a>';
		$katilbize = '<a href="'.filter_var($authUrl, FILTER_SANITIZE_URL).'" class="btn btn-md btn-border c-white">Hemen Katıl!</a>';
		$katilbize2 = '<a href="'.filter_var($authUrl, FILTER_SANITIZE_URL).'" class="btn btn-primary btn-lg" >Üniversitem.Online topluluğuna hemen katıl!</a>';
		
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		
		<title>Üniversitem Online | Türkiyenin Açık Kaynak Sosyal Bilgi Ağı</title>
		
		<!-- Required meta tags always come first -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="Bootstrap/dist/css/bootstrap-reboot.css">
		<link rel="stylesheet" type="text/css" href="Bootstrap/dist/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="Bootstrap/dist/css/bootstrap-grid.css">
		
		<!-- Main Styles CSS -->
		<link rel="stylesheet" type="text/css" href="css/main.min.css">
		<link rel="stylesheet" type="text/css" href="css/fonts.min.css">
		<link rel="stylesheet" type="text/css" href="css/costumize.css">
		
		<!-- Main Font -->
		<script src="js/webfontloader.min.js"></script>
		<script>
			WebFont.load({
				google: {
					families: ['Roboto:300,400,500,700:latin']
				}
			});
		</script>
		
	</head>
	<body class="body-bg-white">
		
		
		<div class="main-header main-header-fullwidth main-header-has-header-standard">
			
			
			<!-- Header Standard Landing  -->
			
			<div class="header--standard header--standard-landing" id="header--standard">
				<div class="container">
					<div class="header--standard-wrap">
						
						<a href="#" class="logo">
							<div class="img-wrap">
								<img src="img/logo.png" alt="Olympus">
								<img src="img/logo-colored-small.png" alt="Olympus" class="logo-colored">
							</div>
							<div class="title-block">
								<h6 class="logo-title">Üniversitem.Online</h6>
								<div class="sub-title">Açık Kaynak Bilgi Platformu</div>
							</div>
						</a>
						
						<a href="#" class="open-responsive-menu js-open-responsive-menu">
							<svg class="olymp-menu-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-menu-icon"></use></svg>
						</a>
						
						
					</div>
				</div>
			</div>
			
			<!-- ... end Header Standard Landing  -->
			<div class="header-spacer--standard"></div>
			
			<div class="content-bg-wrap">
				<div class="content-bg bg-landing"></div>
			</div>
			
			<div class="container">
				<div class="row display-flex">
					<div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-xs-12">
						<div class="landing-content">
							<h1>Türkiye'nin açık kaynak bilgi platformu !</h1>
							
							<p>Universitem.Online öğrenmek ve öğretmek isteyen bilginin paylaştıkça çoğaldığını bilen bir topluluk platformudur. Tamamen ücretsiz eğitime ve eğitmeye aç insanların olduğu bu topluluğa bilginin sosyalleşmiş haline hoşgeldiniz ! </p>
							
							<?php echo $katilbize; ?>
						</div>
					</div>
					
					<div class="col-xl-5 ml-auto col-lg-6 col-md-12 col-sm-12 col-xs-12">
						
						
						<!-- Login-Registration Form  -->
						
						<div class="registration-login-form">
							<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" data-toggle="tab" href="#home" role="tab">
										<svg class="olymp-login-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-login-icon"></use></svg>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#profile" role="tab">
										<svg class="olymp-register-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-register-icon"></use></svg>
									</a>
								</li>
							</ul>
							
							<!-- Tab panes -->
							<div class="tab-content">
								<div class="tab-pane active" id="home" role="tabpanel" data-mh="log-tab">
									<div class="title h6">Login to your Account</div>
									<form class="content">
										<div class="row">
											<div class="col-xl-12 col-lg-12 col-md-12">
												<div class="full-width">
													<p class="pcenter"><img class="img-responsive front-logo" src="img/logo-500.jpg"></p>
												</div>
												
												
												
												
												
												
												
												
												<?php echo $output; ?>
												
												<div class="or"></div>
												
												<a href="/home.php" class="btn btn-lg bg-purple full-width btn-icon-left"><i class="fa fa-external-link-square" aria-hidden="true"></i>Siteye devam et</a>
												<a href="/boss/login/logout.php" class="btn btn-lg bg-purple full-width btn-icon-left"><i class="fa fa-external-link-square" aria-hidden="true"></i>Çıkış Yap</a>
												
												
												
												
												<p>Gmail hesabınız yok mu ? <a href="#">Hemen Gmail Hesabı Aç!</a></br></br> Üzgünüz ama Gmail adresiniz olmadan bir hesabınızda olmayacak ve herhangi bir paylaşımda yapamayacaksınız. Ne yazık ki bu topluluğa katkıda bulunamayacaksınız...</p>
											</div>
										</div>
									</form>
								</div>
								
								
							</div>
						</div>
						
						<!-- ... end Login-Registration Form  -->
					</div>
				</div>
			</div>
			
			<img class="img-bottom" src="img/group-bottom.png" alt="friends">
			
		</div>
		
		
		
		<!-- Clients Block -->
		
		<section class="crumina-module crumina-clients">
			<div class="container">
				<div class="row">
					<div class="col-xl-2 m-auto col-lg-2 col-md-6 col-sm-6 col-xs-12">
						<a class="clients-item" href="#">
							<img src="img/client1.png" class="" alt="logo">
						</a>
					</div>
					<div class="col-xl-2 m-auto col-lg-2 col-md-6 col-sm-6 col-xs-12">
						<a class="clients-item" href="#">
							<img src="img/client2.png" class="" alt="logo">
						</a>
					</div>
					<div class="col-xl-2 m-auto col-lg-2 col-md-6 col-sm-6 col-xs-12">
						<a class="clients-item" href="#">
							<img src="img/client3.png" class="" alt="logo">
						</a>
					</div>
					<div class="col-xl-2 m-auto col-lg-2 col-md-6 col-sm-6 col-xs-12">
						<a class="clients-item" href="#">
							<img src="img/client4.png" class="" alt="logo">
						</a>
					</div>
					<div class="col-xl-2 m-auto col-lg-2 col-md-6 col-sm-6 col-xs-12">
						<a class="clients-item" href="#">
							<img src="img/client5.png" class="" alt="logo">
						</a>
					</div>
				</div>
			</div>
		</section>
		
		<!-- ... end Clients Block -->
		
		
		<!-- Section Img Scale Animation -->
		
		<section class="align-center pt80 section-move-bg-top img-scale-animation scrollme">
			<div class="container">
				<div class="row">
					<div class="col-xl-10 m-auto col-lg-10 col-md-12 col-sm-12 col-xs-12">
						<img class="main-img" src="img/scale1.png" alt="screen">
					</div>
				</div>
				
				<img class="first-img1" alt="img" src="img/scale2.png">
				<img class="second-img1" alt="img" src="img/scale3.png">
				<img class="third-img1" alt="img" src="img/scale4.png">
			</div>
			<div class="content-bg-wrap">
				<div class="content-bg bg-section2"></div>
			</div>
		</section>
		
		<!-- ... end Section Img Scale Animation -->
		
		<section class="medium-padding120">
			<div class="container">
				<div class="row">
					<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
						<img src="img/icon-fly.png" alt="screen">
					</div>
					
					<div class="col-xl-4 col-lg-4 m-auto col-md-12 col-sm-12 col-xs-12">
						<div class="crumina-module crumina-heading">
							<h2 class="heading-title">Nedir bu <span class="c-primary">Üniversitem.Online</span>?</h2>
							<p class="heading-text">Türkiyenin tüm üniversitelerinden öğrencilerin olduğu herhangi bir hocanın ders notundan tutun da video eğitimlerine kadar öğrencilerin birbirlerine yardımcı olduğu sınav notlarının paylaşıldığı üniversiteler arası öğrenci platformudur. Deyip bitirmek isterdik fakat devamı var... Hemen aşağıda !
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="medium-padding120">
			<div class="container">
				<div class="row">
					<div class="col-xl-4 col-lg-4 m-auto col-md-12 col-sm-12 col-xs-12">
						<div class="crumina-module crumina-heading">
							<h2 class="heading-title">Sadece üniversiteliler mi? <span class="c-primary"> Elbette hayır :)</span></h2>
							<p class="heading-text">Genel yapı <span class="c-primary">üniversite öğrencilerine</span> yönelik olsa da amaç üniversite seviyesinde bilgi paylaşımını öğrenmeye hevesli <span class="c-primary">her insana </span>sağlamak. 
								<span class="c-primary">Liseden üniversiteye</span> geçecek bir öğrencinin her üniversite hakkında ulaşabileceği gerçek deneyim ve bilgilerden, <span class="c-primary">vizeler ve finallere </span>hazırlanan bir üniversite öğrencisinin diğer üniversitelerde ki insanlarla not paylaşımına, <span class="c-primary">iş hayatına</span> başlamış bir <span class="c-primary">mezunun</span> bilgilerini tazelemek ve soru sormak için ihtiyaç duyduğu bilgilere kadar <span class="c-primary">herşey burada.</span>
							</p>
						</div>
					</div>
					
					<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
						<img src="img/image1.png" alt="screen">
					</div>
				</div>
			</div>
		</section>
		
		
		<section class="medium-padding120">
			<div class="container">
				<div class="row">
					<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
						<img src="img/image2.png" alt="screen">
					</div>
					
					<div class="col-xl-4 col-lg-4 m-auto col-md-12 col-sm-12 col-xs-12">
						<div class="crumina-module crumina-heading">
							<h2 class="heading-title">Sürekli yaşayan yeni nesil <span class="c-primary">Bilgi Sosyal Ağı</span></h2>
							<p class="heading-text">Siz paylaştıkça değerlenen bu platformda gündelik <span class="c-primary">paylaşımlar</span> yapabilir, <span class="c-primary">blog</span> yazabilir, <span class="c-primary">duyuru</span> oluşturabilir, <span class="c-primary">etkinlik</span> ekleyebilirsiniz. 
								Okulunuzda ki sınıfınız için <span class="c-primary">gruplar</span> oluşturabilir bu gruba sınıftaki diğer arkadaşlarınızı ekleyebilir ve bilgi paylaşımında bulunabilirsiniz.
								Diğer insanların gönderilerini ve bloglarını <span class="c-primary">beğenebilir</span>, <span class="c-primary">favorilerinize</span> ekleyebilirsiniz.
								Ve buraya sığmayacak onlarca özellikle <span class="c-primary">Üniversitem.Online</span> artık sizin !
								
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="medium-padding120">
			<div class="container">
				<div class="row">
					<div class="col-xl-4 col-lg-4 m-auto col-md-12 col-sm-12 col-xs-12">
						<div class="crumina-module crumina-heading">
							<h2 class="heading-title">Eğitim al, iş bul, <span class="c-primary">sosyalleş!</span></h2>
							<p class="heading-text">Oluşturulan etkinliklere katılarak <span class="c-primary">yeni arkadaşlar</span> edin ardından <span class="c-primary">çevreni genişlet</span>. Bu platformda <span class="c-primary">bilgi paylaş</span> ve <span class="c-primary">bilgi al</span> kendini <span class="c-primary">geliş</span>tir-kendini <span class="c-primary">yetiş</span>tir. İçeride bulunan <span class="c-primary">iş ilanları</span>ndan kendine uygun olanı seç <span class="c-primary">staj yap</span> yada <span class="c-primary">işe başla</span>.
							</p>
						</div>
					</div>
					
					<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
						<img src="img/image3.png" alt="screen">
					</div>
				</div>
			</div>
		</section>
		
		
		
		<!-- Planer Animation -->
		
		<section class="medium-padding120 bg-section3 background-cover planer-animation">
			<div class="container">
				<div class="row mb60">
					<div class="col-xl-4 col-lg-4 m-auto col-md-12 col-sm-12 col-xs-12">
						<div class="crumina-module crumina-heading align-center">
							<div class="heading-sup-title">Üniversitem.Online</div>
							<h2 class="h1 heading-title">Topluluk Görüşleri</h2>
							<p class="heading-text">Kullanıcılarımızın Üniversitem.Online hakkındaki görüşlerinden bazıları</p>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="swiper-container pagination-bottom" data-show-items="3">
						<div class="swiper-wrapper">
							<div class="ui-block swiper-slide">
								
								
								<!-- Testimonial Item -->
								
								<div class="crumina-module crumina-testimonial-item">
									<div class="testimonial-header-thumb"></div>
									
									<div class="testimonial-item-content">
										
										<div class="author-thumb">
											<img src="img/avatar3.jpg" alt="author">
										</div>
										
										<h3 class="testimonial-title">Amazing Community</h3>
										
										<ul class="rait-stars">
											<li>
												<i class="fa fa-star star-icon"></i>
											</li>
											<li>
												<i class="fa fa-star star-icon"></i>
											</li>
											
											<li>
												<i class="fa fa-star star-icon"></i>
											</li>
											<li>
												<i class="fa fa-star star-icon"></i>
											</li>
											<li>
												<i class="fa fa-star-o star-icon"></i>
											</li>
										</ul>
										
										<p class="testimonial-message">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
											incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
											exercitation ullamco.
										</p>
										
										<div class="author-content">
											<a href="#" class="h6 author-name">Mathilda Brinker</a>
											<div class="country">Los Angeles, CA</div>
										</div>
									</div>
								</div>
								
								<!-- ... end Testimonial Item -->
							</div>
							
							<div class="ui-block swiper-slide">
								
								
								<!-- Testimonial Item -->
								
								<div class="crumina-module crumina-testimonial-item">
									<div class="testimonial-header-thumb"></div>
									
									<div class="testimonial-item-content">
										
										<div class="author-thumb">
											<img src="img/avatar17.jpg" alt="author">
										</div>
										
										<h3 class="testimonial-title">This is the Best Social Network ever!</h3>
										
										<ul class="rait-stars">
											<li>
												<i class="fa fa-star star-icon"></i>
											</li>
											<li>
												<i class="fa fa-star star-icon"></i>
											</li>
											
											<li>
												<i class="fa fa-star star-icon"></i>
											</li>
											<li>
												<i class="fa fa-star star-icon"></i>
											</li>
											<li>
												<i class="fa fa-star star-icon"></i>
											</li>
										</ul>
										
										<p class="testimonial-message">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
											nulla pariatur laborum.
										</p>
										
										<div class="author-content">
											<a href="#" class="h6 author-name">Marina Valentine</a>
											<div class="country">Long Island, NY</div>
										</div>
									</div>
								</div>
								
								<!-- ... end Testimonial Item -->
								
							</div>
							
							<div class="ui-block swiper-slide">
								
								
								<!-- Testimonial Item -->
								
								<div class="crumina-module crumina-testimonial-item">
									<div class="testimonial-header-thumb"></div>
									
									<div class="testimonial-item-content">
										
										<div class="author-thumb">
											<img src="img/avatar1.jpg" alt="author">
										</div>
										
										<h3 class="testimonial-title">Incredible Design!</h3>
										
										<ul class="rait-stars">
											<li>
												<i class="fa fa-star star-icon"></i>
											</li>
											<li>
												<i class="fa fa-star star-icon"></i>
											</li>
											
											<li>
												<i class="fa fa-star star-icon"></i>
											</li>
											<li>
												<i class="fa fa-star star-icon"></i>
											</li>
											<li>
												<i class="fa fa-star-o star-icon"></i>
											</li>
										</ul>
										
										<p class="testimonial-message">Sed ut perspiciatis unde omnis iste natus error sit
											voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab
											illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
										</p>
										
										<div class="author-content">
											<a href="#" class="h6 author-name">Nicholas Grissom</a>
											<div class="country">San Francisco, CA</div>
										</div>
									</div>
								</div>
								
								<!-- ... end Testimonial Item -->
							</div>
						</div>
						
						<div class="swiper-pagination"></div>
					</div>
				</div>
			</div>
			
			<img src="img/planer.png" alt="planer" class="planer">
		</section>
		
		<!-- ... end Section Planer Animation -->
		
		<section class="medium-padding120">
			<div class="container">
				<div class="row">
					<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
						<img src="img/image4.png" alt="screen">
					</div>
					
					<div class="col-xl-5 col-lg-5 m-auto col-md-12 col-sm-12 col-xs-12">
						<div class="crumina-module crumina-heading">
							<h2 class="h1 heading-title">Çok yakında <span class="c-primary">Google Play</span> ve <span class="c-primary">Apple Store</span>'da!</h2>
							<p class="heading-text">Üniversitem.Online çok yakında telefon ve tabletleriniz için özenle hazırlanan uygulamalarla desteklenecek ve bilgiye erişim çok daha kolay olacak.
							</p>
						</div>
						
						
						<ul class="list--styled">
							<li>
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								Yepyeni bir sosyal platform.
							</li>
							<li>
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								Yepyeni bir dünya!
							</li>
						</ul>
						
						<a href="" class="btn btn-market">
							<img class="icon" src="svg-icons/apple-logotype.svg" alt="app store">
							<div class="text">
								<span class="sup-title">YAKINDA</span>
								<span class="title">App Store</span>
							</div>
						</a>
						
						<a href="" class="btn btn-market">
							<img class="icon" src="svg-icons/google-play.svg" alt="google">
							<div class="text">
								<span class="sup-title">YAKINDA</span>
								<span class="title">Google Play</span>
							</div>
						</a>
					</div>
				</div>
			</div>
		</section>
		
		
		<!-- Section Subscribe Animation -->
		
		<section class="medium-padding100 subscribe-animation scrollme bg-users">
			<div class="container">
				<div class="row">
					<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
						<div class="crumina-module crumina-heading c-white custom-color">
							<h2 class="h1 heading-title">Haberdar Ol</h2>
							<p class="heading-text">Duyuru bültenimize katılıp tüm gelişmelerden haberdar olmak için mail adresinizi bırakın!
							</p>
						</div>
						
						
						<!-- Subscribe Form  -->
						
						<form class="form-inline subscribe-form" method="post">
							<div class="form-group label-floating is-empty">
								<label class="control-label">Lütfen kullandığınız bir mail adresini giriniz...</label>
								<input class="form-control bg-white" placeholder="" type="email">
							</div>
							
							<button class="btn btn-blue btn-lg">Haberdar ol</button>
						</form>
						
						<!-- ... end Subscribe Form  -->
						
					</div>
				</div>
				
				<img src="img/paper-plane.png" alt="plane" class="plane">
			</div>
		</section>
		
		<!-- ... end Section Subscribe Animation -->
		<section class="medium-padding120">
			<div class="container">
				<div class="row mb60">
					<div class="col-xl-4 col-lg-4 m-auto col-md-12 col-sm-12 col-xs-12">
						<div class="crumina-module crumina-heading align-center">
							<div class="heading-sup-title">ÜNİVERSİTEM.ONLİNE</div>
							<h2 class="h1 heading-title">Son Yazıları</h2>
							<p class="heading-text">Kullanıcılarımızın ürettiği son içerikler anında anasayfada</p>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12">
						
						
						<!-- Post -->
						
						<article class="hentry blog-post">
							
							<div class="post-thumb">
								<img src="img/post1.jpg" alt="photo">
							</div>
							
							<div class="post-content">
								<a href="#" class="post-category bg-blue-light">THE COMMUNITY</a>
								<a href="#" class="h4 post-title">Here’s the Featured Urban photo of August! </a>
								<p>Here’s a photo from last month’s photoshoot. We got really awesome shots for the new catalog.</p>
								
								<div class="author-date">
									by
									<a class="h6 post__author-name fn" href="#">Maddy Simmons</a>
									<div class="post__date">
										<time class="published" datetime="2017-03-24T18:18">
											- 7 hours ago
										</time>
									</div>
								</div>
								
								<div class="post-additional-info inline-items">
									
									<ul class="friends-harmonic">
										<li>
											<a href="#">
												<img src="img/icon-chat27.png" alt="icon">
											</a>
										</li>
										<li>
											<a href="#">
												<img src="img/icon-chat2.png" alt="icon">
											</a>
										</li>
									</ul>
									<div class="names-people-likes">
										26
									</div>
									
									<div class="comments-shared">
										<a href="#" class="post-add-icon inline-items">
											<svg class="olymp-speech-balloon-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use></svg>
											<span>0</span>
										</a>
									</div>
									
								</div>
							</div>
							
						</article>
						
						<!-- ... end Post -->
					</div>
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12">
						
						
						<!-- Post -->
						
						<article class="hentry blog-post">
							
							<div class="post-thumb">
								<img src="img/post2.jpg" alt="photo">
							</div>
							
							<div class="post-content">
								<a href="#" class="post-category bg-primary">OLYMPUS NEWS</a>
								<a href="#" class="h4 post-title">Olympus Network added new photo filters!</a>
								<p>Here’s a photo from last month’s photoshoot. We got really awesome shots for the new catalog.</p>
								
								<div class="author-date">
									by
									<a class="h6 post__author-name fn" href="#">JACK SCORPIO</a>
									<div class="post__date">
										<time class="published" datetime="2017-03-24T18:18">
											- 12 hours ago
										</time>
									</div>
								</div>
								
								<div class="post-additional-info inline-items">
									
									<ul class="friends-harmonic">
										<li>
											<a href="#">
												<img src="img/icon-chat4.png" alt="icon">
											</a>
										</li>
										<li>
											<a href="#">
												<img src="img/icon-chat26.png" alt="icon">
											</a>
										</li>
										<li>
											<a href="#">
												<img src="img/icon-chat16.png" alt="icon">
											</a>
										</li>
									</ul>
									<div class="names-people-likes">
										82
									</div>
									
									<div class="comments-shared">
										<a href="#" class="post-add-icon inline-items">
											<svg class="olymp-speech-balloon-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use></svg>
											<span>14</span>
										</a>
									</div>
									
								</div>
							</div>
							
						</article>
						
						<!-- ... end Post -->
					</div>
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12">
						
						
						<!-- Post -->
						
						<article class="hentry blog-post">
							
							<div class="post-thumb">
								<img src="img/post3.jpg" alt="photo">
							</div>
							
							<div class="post-content">
								<a href="#" class="post-category bg-purple">INSPIRATION</a>
								<a href="#" class="h4 post-title">Take a look at these truly awesome worspaces</a>
								<p>Here’s a photo from last month’s photoshoot. We got really awesome shots for the new catalog.</p>
								
								<div class="author-date">
									by
									<a class="h6 post__author-name fn" href="#">Maddy Simmons</a>
									<div class="post__date">
										<time class="published" datetime="2017-03-24T18:18">
											- 2 days ago
										</time>
									</div>
								</div>
								
								<div class="post-additional-info inline-items">
									
									<ul class="friends-harmonic">
										<li>
											<a href="#">
												<img src="img/icon-chat28.png" alt="icon">
											</a>
										</li>
									</ul>
									<div class="names-people-likes">
										0
									</div>
									
									<div class="comments-shared">
										<a href="#" class="post-add-icon inline-items">
											<svg class="olymp-speech-balloon-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use></svg>
											<span>22</span>
										</a>
									</div>
									
								</div>
							</div>
							
						</article>
						
						<!-- ... end Post -->
					</div>
				</div>
			</div>
		</section>
		
		
		<!-- Section Call To Action Animation -->
		
		<section class="align-right pt160 pb80 section-move-bg call-to-action-animation scrollme">
			<div class="container">
				<div class="row">
					<div class="col-xl-10 m-auto col-lg-10 col-md-12 col-sm-12 col-xs-12">
						<?php echo $katilbize2; ?>
					</div>
				</div>
			</div>
			<img class="first-img" alt="guy" src="img/guy.png">
			<img class="second-img" alt="rocket" src="img/rocket1.png">
			<div class="content-bg-wrap">
				<div class="content-bg bg-section1"></div>
			</div>
		</section>
		
		<!-- ... end Section Call To Action Animation -->
		
		
		<div class="modal fade" id="registration-login-form-popup">
			<div class="modal-dialog ui-block window-popup registration-login-form-popup">
				<a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
					<svg class="olymp-close-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
				</a>
				<div class="registration-login-form">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#home1" role="tab">
								<svg class="olymp-login-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-login-icon"></use></svg>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#profile1" role="tab">
								<svg class="olymp-register-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-register-icon"></use></svg>
							</a>
						</li>
					</ul>
					
					<!-- Tab panes -->
					<div class="tab-content">
						
						
						<div class="tab-pane" id="profile1" role="tabpanel" data-mh="log-tab">
						</div>
					</div>
				</div>
			</div>
		</div>
		
	<?php include 'footer.php'; ?>										