
<div class="your-profile">
    <div class="ui-block-title ui-block-title-small">
        <h6 class="title">Profilim</h6>
    </div>

    <div class="ui-block-title">
        <a href="profil.php" class="h6 title">Kişisel Bilgiler</a>
    </div>
    <div class="ui-block-title">
        <a href="profile-hobbies-interest.php" class="h6 title">Hobiler ve İlgi Alanları</a>
    </div>

    <div class="ui-block-title">
        <a href="33-YourAccount-Notifications.html" class="h6 title">Bildirimler</a>
        <a href="#" class="items-round-little bg-primary">8</a>
    </div>
    <div class="ui-block-title">
        <a href="34-YourAccount-ChatMessages.html" class="h6 title">Mesajlarım</a>
    </div>
    <div class="ui-block-title">
        <a href="35-YourAccount-FriendsRequests.html" class="h6 title">Arkadaşlık İstekleri</a>
        <a href="#" class="items-round-little bg-blue">4</a>
    </div>
    <div class="ui-block-title ui-block-title-small">
        <h6 class="title">Aktiviteler</h6>
    </div>
    <div class="ui-block-title">
        <a href="36-FavPage-SettingsAndCreatePopup.html" class="h6 title">Beğendiklerim</a>
    </div>
    <div class="ui-block-title">
        <a href="36-FavPage-SettingsAndCreatePopup.html" class="h6 title">Kaydettiklerim</a>
    </div>
    <div class="ui-block-title">
        <a href="36-FavPage-SettingsAndCreatePopup.html" class="h6 title">Yorumlarım</a>
    </div>
</div>
