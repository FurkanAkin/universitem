	<div class="header-spacer header-spacer-small"></div>
	
	<!-- Main Header Account -->
	
	<div class="main-header">
		<div class="content-bg-wrap">
			<div class="content-bg bg-account"></div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-8 m-auto col-md-8 col-sm-12 col-xs-12">
					<div class="main-header-content">
						<h1>Profil Ayarları</h1>
						<p>Profil ayarları bölümüne hoşgeldiniz. Eksik yada hatalı bilgilerinizi bu bölümden düzenleyebilirsiniz.</p>
					</div>
				</div>
			</div>
		</div>
		<img class="img-bottom" src="img/account-bottom.png" alt="friends">
		</div>
	
	<!-- ... end Main Header Account -->
	
	
	<!-- Your Account Personal Information -->
	
	<div class="container">
		<div class="row">
			<div class="col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
				<div class="ui-block">
					<div class="ui-block-title">
						<h6 class="title">Kişisel Bilgilerim</h6>
					</div>
					<div class="ui-block-content">
						
						
						<!-- Personal Information Form  -->
						<div id="profilGuncelleAlert"></div>
						<form id="profilGuncelleForm">
							<input type="hidden" value="<?php echo $users['users_id']; ?>" name="users_id">
							<div class="row">
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<div class="form-group label-floating <?php profilduzenlebossa('users_youtube'); ?>">
										<label class="control-label">Adınız</label>
										<input name="users_name" class="form-control" placeholder="" type="text"
										value="<?php echo $users['users_name']; ?>">
									</div>
									
									<div class="form-group label-floating <?php profilduzenlebossa('users_youtube'); ?>">
										<label class="control-label">Email Adresiniz</label>
										<input readonly name="users_email" class="form-control" placeholder="" type="email"
										value="<?php echo $users['users_email']; ?>">
									</div>
									
									<div class="form-group date-time-picker label-floating <?php profilduzenlebossa('users_youtube'); ?>">
										<label class="control-label">Doğum Tarihiniz</label>
										<input name="users_birth" value="<?php echo $users['users_birth']; ?>"/>
										<span class="input-group-addon">
											<svg class="olymp-month-calendar-icon icon"><use
											xlink:href="svg-icons/sprites/icons.svg#olymp-month-calendar-icon"></use></svg>
										</span>
									</div>
								</div>
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<div class="form-group label-floating <?php profilduzenlebossa('users_youtube'); ?>">
										<label class="control-label">Soy İsminiz</label>
										<input class="form-control" placeholder="" type="text"
										name="users_surname" value="<?php echo $users['users_surname']; ?>">
									</div>
									
									<div class="form-group label-floating <?php profilduzenlebossa('users_youtube'); ?>">
										<label class="control-label">TC Kimlik Numaranız</label>
										<input class="form-control" placeholder="" type="text"
										name="users_idcode" value="<?php echo $users['users_idcode']; ?>">
									</div>
									
									
									<div class="form-group label-floating <?php profilduzenlebossa('users_youtube'); ?>">
										<label class="control-label">Telefon Numaranız</label>
										<input class="form-control" placeholder="" type="text"
										name="users_phone" value="<?php echo $users['users_phone']; ?>">
									</div>
									
								</div>
								
								<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
									<div class="form-group label-floating is-select">
										<label class="control-label">Ülkeniz</label>
										<select class="selectpicker form-control" name="users_provider_locale">
											
											<?php
												
												
												if ($users['users_provider_locale'] == 'tr') { ?>
												<option  value="tr">Türkiye</option>
												<?php } else {
												?>
												<option  value="oth">Other</option>
												<?php }
											?>
											<option  value="oth">Other</option>
										</select>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
									<div class="form-group label-floating is-select">
										<label class="control-label">Şehir</label>
										<select class="selectpicker form-control" name="users_city">
										<?php 
										
 ?>
										
										<option value="<?php echo $users['users_city']; ?>"><?php echo iller($users['users_city']); ?></option>
										<?php	for($i=0;$i<81;$i++){ ?>
											<option value="<?php echo $i ?>"><?php echo iller($i);?></option>
										<?php } ?>
										
										</select>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
									<div class="form-group label-floating is-select">
										<label class="control-label">İlçe</label>
										<select class="selectpicker form-control" name="users_district">
											<option value="SF">Pendik</option>
											<option value="NY">Kadıköy</option>
										</select>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<div class="form-group label-floating <?php profilduzenlebossa('users_youtube'); ?>">
										<label class="control-label">Kısaca hakkınızda</label>
										<textarea class="form-control" name="users_shortdesc" placeholder=""><?php echo $users['users_shortdesc']; ?></textarea>
									</div>
									
									<div class="form-group label-floating is-select">
										<label class="control-label">Cinsiyetiniz</label>
										<select class="selectpicker form-control" name="users_gender">
											<?php
												if ($users['users_gender'] == 'male') { ?>
												<option  value="male">Erkek</option>
												<option  value="female">Kadın</option>
												<?php
													} elseif($users['users_gender'] == 'female') {
													
												?>
												<option  value="female">Kadın</option>
												<option  value="male">Erkek</option>
												<?php }else{  ?>
												<option  value="ukn">Belirtilmemiş</option>
												<option  value="male">Erkek</option>
												<option  value="female">Kadın</option>
											<?php } ?>
										</select>
									</div>
									
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<div class="form-group label-floating <?php profilduzenlebossa('users_youtube'); ?>">
										<label class="control-label">Okulunuz</label>
										<input name="users_school" class="form-control" placeholder="" type="text" value="<?php echo $users['users_school']; ?>">
									</div>
									
									<div class="form-group label-floating <?php profilduzenlebossa('users_youtube'); ?>">
										<label class="control-label">Mesleğiniz</label>
										<input name="users_job" class="form-control" placeholder="" type="text" value="<?php echo $users['users_job']; ?>">
									</div>
									
									<div class="form-group label-floating is-select">
										<label class="control-label">Çalışma Durumunuz</label>
										<select class="selectpicker form-control">
											<option value="stu">Öğrenci</option>
											<option value="wrk">Çalışıyor</option>
											<option value="uwrk">Çalışmıyor</option>
										</select>
									</div>
									
								</div>
								<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="form-group with-icon label-floating <?php profilduzenlebossa('users_youtube'); ?>">
										<label class="control-label">Facebook Adresiniz</label>
										<input name="users_facebook" class="form-control" type="text" value="<?php echo $users['users_facebook']; ?>">
										<i class="fa fa-facebook c-facebook" aria-hidden="true"></i>
									</div>
									<div class="form-group with-icon label-floating <?php profilduzenlebossa('users_youtube'); ?>">
										<label class="control-label">Twitter Adresiniz</label>
										<input name="users_twitter" class="form-control" type="text" value="<?php echo $users['users_twitter']; ?>">
										<i class="fa fa-twitter c-twitter" aria-hidden="true"></i>
									</div>
									<div class="form-group with-icon label-floating <?php profilduzenlebossa('users_youtube'); ?>">
										<label class="control-label">Instagram Adresiniz</label>
										<input name="users_instagram" class="form-control" type="text" value="<?php echo $users['users_instagram']; ?>">
										<i class="fa fa-instagram c-instagram" aria-hidden="true"></i>
									</div>
									<div class="form-group with-icon label-floating <?php profilduzenlebossa('users_youtube'); ?>">
										<label class="control-label">Web Siteniz</label>
										<input name="users_website" class="form-control" type="text" value="<?php echo $users['users_website']; ?>">
										<i class="fa fa-rss c-rss" aria-hidden="true"></i>
									</div>
									<div class="form-group with-icon label-floating <?php profilduzenlebossa('users_youtube'); ?>">
										<label class="control-label">Youtube Kanalınız</label>
										<input name="users_youtube" class="form-control" type="text" value="<?php echo $users['users_youtube']; ?>">
										<i class="fa fa-youtube c-youtube" aria-hidden="true"></i>
									</div>
									
								</div>
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<a id="profilGuncelleBtn" class="btn btn-primary btn-lg full-width" style="color: #fff;">Profili Kaydet</a>
								</div>
								
							</div>
						</form>
						
						<!-- ... end Personal Information Form  -->
					</div>
				</div>
			</div>
			
			<div class="col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12 col-xs-12 responsive-display-none">
				<div class="ui-block">
					
					
					<!-- Your Profile  -->
<?php include_once "modules/profil-settings.php";?>
					<!-- ... end Your Profile  -->
					
					
				</div>
			</div>
		</div>
	</div>
	