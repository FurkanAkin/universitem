

<!-- ... end Top Header-Profile -->

<div class="container">
	<div class="row">
		<div class="col-xl-8 order-xl-2 col-lg-8 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title"> Hobiler ve İlgi Alanları </h6>
					<div class="more">
                                            <svg class="olymp-three-dots-icon">
                                                <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                            </svg>
                                            <ul class="more-dropdown more-with-triangle">
                                                <li>
                                                    <a href="profile-settings.php">Düzenle</a>
                                                </li>
                                            </ul>
                                        </div>
				</div>
				<div class="ui-block-content">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

							
							<!-- W-Personal-Info -->
							
							<ul class="widget w-personal-info item-block">
								<li>
									<span class="title">Hobiler:</span>
									<span class="text"><?php echo $users['users_fav_hobbies'];?>
														</span>
								</li>
								<li>
									<span class="title">Favori Diziler:</span>
									<span class="text"><?php echo $users['users_fav_series'];?></span>
								</li>
								<li>
									<span class="title">Favori Filmler:</span>
									<span class="text"><?php echo $users['users_fav_movies'];?> </span>
								</li>
								<li>
									<span class="title">Favori Spor Dalları:</span>
									<span class="text"><?php echo $users['users_fav_sports'];?> </span>
                                </li>
                                <li>
									<span class="title">Favori Oyunlar:</span>
									<span class="text"><?php echo $users['users_fav_games'];?> </span>
								</li>
							</ul>
							
							<!-- ... end W-Personal-Info -->
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

							
							<!-- W-Personal-Info -->
							
							<ul class="widget w-personal-info item-block">
								<li>
									<span class="title">Favori Müzik Grupları ve Sanatçılar:</span>
									<span class="text"><?php echo $users['users_fav_musics'];?></span>
								</li>
								<li>
									<span class="title">Favori Kitaplar:</span>
									<span class="text"><?php echo $users['users_fav_books'];?></span>
								</li>
								<li>
									<span class="title">Favori Dergiler:</span>
									<span class="text"><?php echo $users['users_fav_journal'];?> </span>
								</li>
								<li>
									<span class="title">Favori Etkinlikler:</span>
									<span class="text"><?php echo $users['users_fav_events'];?></span>
                                </li>								
                                <li>
									<span class="title">Diğer İlgi Alanları:</span>
									<span class="text"><?php echo $users['users_other_hobbies'];?></span>
								</li>							</ul>
							
							<!-- ... end W-Personal-Info -->
						</div>
					</div>
				</div>
			</div>
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Eğitim ve İş</h6>
					<div class="more">
                                            <svg class="olymp-three-dots-icon">
                                                <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                            </svg>
                                            <ul class="more-dropdown more-with-triangle">
                                                <li>
                                                    <a href="profile-settings.php">Düzenle</a>
                                                </li>
                                            </ul>
                                        </div>
				</div>
				<div class="ui-block-content">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

							
							<!-- W-Personal-Info -->
							
							<ul class="widget w-personal-info item-block">
								<li>
									<span class="title">The New College of Design</span>
									<span class="date">2001 - 2006</span>
									<span class="text">Breaking Good, RedDevil, People of Interest, The Running Dead, Found,  American Guy.</span>
								</li>
								<li>
									<span class="title">Rembrandt Institute</span>
									<span class="date">2008</span>
									<span class="text">Five months Digital Illustration course. Professor: Leonardo Stagg.</span>
								</li>
								<li>
									<span class="title">The Digital College </span>
									<span class="date">2010</span>
									<span class="text">6 months intensive Motion Graphics course. After Effects and Premire. Professor: Donatello Urtle. </span>
								</li>
							</ul>
							
							<!-- ... end W-Personal-Info -->

						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

							
							<!-- W-Personal-Info -->
							
							<ul class="widget w-personal-info item-block">
								<li>
									<span class="title">Digital Design Intern</span>
									<span class="date">2006-2008</span>
									<span class="text">Digital Design Intern for the “Multimedz” agency. Was in charge of the communication with the clients.</span>
								</li>
								<li>
									<span class="title">UI/UX Designer</span>
									<span class="date">2008-2013</span>
									<span class="text">UI/UX Designer for the “Daydreams” agency. </span>
								</li>
								<li>
									<span class="title">Senior UI/UX Designer</span>
									<span class="date">2013-Now</span>
									<span class="text">Senior UI/UX Designer for the “Daydreams” agency. I’m in charge of a ten person group, overseeing all the proyects and talking to potential clients.</span>
								</li>
							</ul>
							
							<!-- ... end W-Personal-Info -->
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xl-4 order-xl-1 col-lg-4 order-lg-1 col-md-12 order-md-2 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Kişisel Bilgiler</h6>
					<div class="more">
                                            <svg class="olymp-three-dots-icon">
                                                <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                            </svg>
                                            <ul class="more-dropdown more-with-triangle">
                                                <li>
                                                    <a href="profile-settings.php">Düzenle</a>
                                                </li>
                                            </ul>
                                        </div>
				</div>
				<div class="ui-block-content">

					
					<!-- W-Personal-Info -->
					
					<ul class="widget w-personal-info">
						<li>
							<span class="title">Hakkımda:</span>
							<span class="text"><?php echo $users['users_shortdesc']; ?>
												</span>
						</li>
						<li>
							<span class="title">Doğum Tarihim:</span>
							<span class="text"><?php echo $users['users_birth']; ?></span>
						</li>

						<li>
							<span class="title">Yaşadığım Yer:</span>
							<span class="text"><?php echo iller($users['users_city']); ?></span>
						</li>
						<li>
							<span class="title">Mesleğim:</span>
							<span class="text"><?php echo $users['users_job']; ?></span>
						</li>
						<li>
							<span class="title">Katılma Tarihi:</span>
							<span class="text"><?php echo $users['users_created_date']; ?></span>
						</li>
						<li>
                            <span class="title">Cinsiyet:</span>
                            <?php if ($users['users_gender'] == 'male') { ?>
							<span class="text">Erkek</span>
							<?php } elseif($users['users_gender'] == 'female') {?>
                            <span class="text">Kadın</span>
							<?php  } ?>

						</li>
						<li>
							<span class="title">Email:</span>
							<span class="text"><?php echo $users['users_email']; ?></span>
                        </li>
                        <?php
                        if ($users['users_website']) {
                            ?>
						<li>
							<span class="title">Website:</span>
							<span class="text"><?php echo $users['users_website']; ?></span>
						</li>
						<?php } ?>
					</ul>
					
					<!-- ... end W-Personal-Info -->
					<!-- W-Socials -->
					
					<div class="widget w-socials">
						<h6 class="title">Sosyal Medya Hesaplarım:</h6>
						<?php
                        if ($users['users_facebook']) {
                            ?>
                            <a href="<?php echo $users['users_facebook']; ?>" class="social-item bg-facebook">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                Facebook
                            </a>
                        <?php } ?>
                        <?php
                        if ($users['users_twitter']) {
                            ?>
                            <a href="<?php echo $users['users_twitter']; ?>" class="social-item bg-twitter">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                Twitter
                            </a>
                        <?php } ?>
                        <?php
                        if ($users['users_instagram']) {
                            ?>
                            <a href="<?php echo $users['users_instagram']; ?>" class="social-item btn-purple">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                                Instagram
                            </a>
                        <?php } ?>
                        <?php
                        if ($users['users_youtube']) {
                            ?>
                            <a href="<?php echo $users['users_youtube']; ?>" class="social-item bg-google">
                                <i class="fa fa-youtube" aria-hidden="true"></i>
                                YouTube
                            </a>
                        <?php } ?>
                         <?php
                        if ($users['users_website']) {
                            ?>
                            <a href="<?php echo $users['users_website']; ?>" class="social-item btn-grey">
                                <i class="fa fa-globe" aria-hidden="true"></i>
                                Website
                            </a>
                        <?php } ?>
					</div>
					
					
					<!-- ... end W-Socials -->
				</div>
			</div>
		</div>
	</div>
</div>