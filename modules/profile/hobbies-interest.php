<div class="header-spacer header-spacer-small"></div>

<!-- Main Header Account -->

<div class="main-header">
    <div class="content-bg-wrap">
        <div class="content-bg bg-account"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 m-auto col-md-8 col-sm-12 col-xs-12">
                <div class="main-header-content">
                    <h1>Hobileri ve İlgi Alanları</h1>
                    <p>Okuduğun kitaplar, takip ettiğin dergiler, izlediğin dizi ve filmler, tuttuğun takım,
                        ilgilendiğin spor dalları, oynamayı sevdiğin oyunlar ve uğraştığın herşey.</p>
                </div>
            </div>
        </div>
    </div>
    <img class="img-bottom" src="img/account-bottom.png" alt="friends">
</div>

<!-- ... end Main Header Account -->


<!-- Your Account Personal Information -->

<div class="container">
    <div class="row">
        <div class="col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">Hobiler ve İlgi Alanları</h6>
                </div>
                <div class="ui-block-content">


                    <!-- Form Hobbies and Interests -->

                    <div id="hobiGuncelleAlert"></div>
                    <form id="hobiGuncelleForm">
                        <input type="hidden" value="<?php echo $users['users_id']; ?>" name="users_id">
                        <div class="row">

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Hobiler</label>
                                    <textarea class="form-control" name="users_fav_hobbies" placeholder=""><?php echo $users['users_fav_hobbies'];?></textarea>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Favori Diziler</label>
                                    <textarea class="form-control" name="users_fav_series" placeholder=""><?php echo $users['users_fav_series'];?></textarea>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Favori Filmler</label>
                                    <textarea class="form-control" name="users_fav_movies" placeholder=""><?php echo $users['users_fav_movies'];?></textarea>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Favori Spor Dalları</label>
                                    <textarea class="form-control" name="users_fav_sports" placeholder=""><?php echo $users['users_fav_sports'];?></textarea>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Favori Oyunlar</label>
                                    <textarea class="form-control" name="users_fav_games" placeholder=""><?php echo $users['users_fav_games'];?></textarea>
                                </div>


                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Favori Müzik Grupları ve Sanatçılar</label>
                                    <textarea class="form-control" name="users_fav_musics" placeholder=""><?php echo $users['users_fav_musics'];?></textarea>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Favori Kitaplar</label>
                                    <textarea class="form-control" name="users_fav_books" placeholder=""><?php echo $users['users_fav_books'];?></textarea>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Favori Dergiler</label>
                                    <textarea class="form-control" name="users_fav_journal" placeholder=""><?php echo $users['users_fav_journal'];?></textarea>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Favori Etkinlikler</label>
                                    <textarea class="form-control" name="users_fav_events" placeholder=""><?php echo $users['users_fav_events'];?></textarea>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Diğer İlgi Alanları</label>
                                    <textarea class="form-control" name="users_other_hobbies" placeholder=""><?php echo $users['users_other_hobbies'];?></textarea>
                                </div>

                                <a id="hobiGuncelleBtn" class="btn btn-primary btn-lg full-width" style="color: #fff;">Tümünü Kaydet</a>
                            </div>

                        </div>
                    </form>

                    <!-- ... end Form Hobbies and Interests -->

                </div>
            </div>
        </div>

        <div class="col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12 col-xs-12 responsive-display-none">
            <div class="ui-block">
                <!-- Your Profile  -->
                <?php include_once "modules/profil-settings.php"; ?>
                <!-- ... end Your Profile  -->
            </div>
        </div>
    </div>
</div>

<!-- ... end Your Account Personal Information -->

