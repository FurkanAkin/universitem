<div class="container">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="ui-block">
                <div class="top-header">
                    <div class="top-header-thumb">
                        <img src="<?php echo $kullanicicek['users_up_picture']; ?>" alt="nature">
                    </div>
                    <div class="profile-section">
                        <div class="row">
                            <div class="col-lg-5 col-md-5 ">
                                <ul class="profile-menu">
                                    <li>
                                        <a href="02-ProfilePage.html" class="active">Duvarım</a>
                                    </li>
                                    <li>
                                        <a href="users-about.php?users_id=<?php echo $kullanicicek['users_id']; ?>">Hakkımda</a>
                                    </li>
                                    <li>
                                        <a href="06-ProfilePage.html">Ağım</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-5 ml-auto col-md-5">
                                <ul class="profile-menu">
                                    <li>
                                        <a href="07-ProfilePage-Photos.html">Fotoğraflar</a>
                                    </li>
                                    <li>
                                        <a href="09-ProfilePage-Videos.html">Videolar</a>
                                    </li>
                                    <li>
                                        <div class="more">
                                            <svg class="olymp-three-dots-icon">
                                                <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                            </svg>
                                            <ul class="more-dropdown more-with-triangle">
                                                <li>
                                                    <a href="#">Report Profile</a>
                                                </li>
                                                <li>
                                                    <a href="#">Block Profile</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="control-block-button">
                        <span class="notification-icon">
									<a href="#" class="accept-request-edit">
										<span class="icon-add without-text">
											<svg class="olymp-happy-face-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="svg-icons/sprites/icons.svg#olymp-happy-face-icon"></use></svg>
										</span>
									</a>
									
									<a href="#" class="accept-request-edit request-del">
										<span class="icon-minus">
											<svg class="olymp-happy-face-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="svg-icons/sprites/icons.svg#olymp-happy-face-icon"></use></svg>
										</span>
									</a>
									
								</span>
                            <a href="#" class="btn btn-control bg-purple">
                                <svg class="olymp-chat---messages-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-chat---messages-icon"></use>
                                </svg>
                            </a>
                            
                            <div class="btn btn-control bg-primary more">
                                <svg class="olymp-settings-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-settings-icon"></use>
                                </svg>

                                <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#update-header-photo">Profil
                                            Fotoğrafını Güncelle</a>
                                    </li>
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#update-header-photo">Kapak
                                            Fotoğrafını Güncelle</a>
                                    </li>
                                    <li>
                                        <a href="29-YourAccount-AccountSettings.html">Hesap Ayarları</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="top-header-author">
                        <a href="02-ProfilePage.html" class="author-thumb profilepp">
                            <img src="<?php echo $kullanicicek['users_picture']; ?>" alt="author">
                        </a>
                        <div class="author-content">
                            <a href="02-ProfilePage.html"
                               class="h4 author-name"><?php echo $kullanicicek['users_name'] . " " . $kullanicicek['users_surname']; ?></a>
                            <div class="country"><?php echo $kullanicicek['users_job']; ?></div>
                            <div class="country"><?php echo $kullanicicek['users_school']; ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>