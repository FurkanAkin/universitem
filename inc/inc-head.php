<?php
	include_once 'boss/login/session.php';


?>
<!DOCTYPE html>
<html lang="tr">
	<head>
		
		<title>Üniversitem.Online | Kullanıcı Profili</title>
		
		<!-- Required meta tags always come first -->
        <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="Bootstrap/dist/css/bootstrap-reboot.css">
		<link rel="stylesheet" type="text/css" href="Bootstrap/dist/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="Bootstrap/dist/css/bootstrap-grid.css">
		
		<!-- Main Styles CSS -->
		<link rel="stylesheet" type="text/css" href="css/main.min.css">
		<link rel="stylesheet" type="text/css" href="css/fonts.min.css">
		<link rel="stylesheet" type="text/css" href="css/costumize.css">

		<!-- Main Font -->
        <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
		<script src="js/webfontloader.min.js"></script>
		<script>
			WebFont.load({
				google: {
					families: ['Roboto:300,400,500,700:latin']
				}
			});
		</script>

		
	</head>