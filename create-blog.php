<?php
include_once 'inc/inc-head.php';

sessionyoksaindexegit();
//KULLANICI GETİR
$users_id = $userData['users_id'];
foreach (kullanicigetir($users_id) as $users) ;

?>

<body>


<!-- Fixed Sidebar Left -->
<?php include_once 'modules/left-sidebar.php'; ?>
<!-- ... end Fixed Sidebar Left -->


<!-- Fixed Sidebar Right -->
<?php include_once 'modules/right-sidebar.php'; ?>
<!-- ... end Fixed Sidebar Right-Responsive -->

<!-- Header-BP -->
<?php include_once 'modules/header-modules.php'; ?>
<!-- ... end Responsive Header-BP -->
<!-- Friends -->
<div class="main-header">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h2 class="presentation-margin">Blog Ekle</h2>
        <div class="ui-block">
            <!-- News Feed Form  -->

            <div class="news-feed-form">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active inline-items" data-toggle="tab" href="#blog" role="tab"
                           aria-expanded="false">
                            <svg class="olymp-blog-icon">
                                <use xlink:href="svg-icons/sprites/icons.svg#olymp-blog-icon"></use>
                            </svg>

                            <span>Blog</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link inline-items" data-toggle="tab" href="#notice" role="tab"
                           aria-expanded="true">

                            <svg class="olymp-status-icon">
                                <use xlink:href="svg-icons/sprites/icons.svg#olymp-status-icon"></use>
                            </svg>

                            <span>Duyuru</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link inline-items" data-toggle="tab" href="#event" role="tab"
                           aria-expanded="false">

                            <svg class="olymp-multimedia-icon">
                                <use xlink:href="svg-icons/sprites/icons.svg#olymp-multimedia-icon"></use>
                            </svg>

                            <span>Etkinlik</span>
                        </a>
                    </li>

                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="blog" role="tabpanel" aria-expanded="true">
                        <div id="blogEkleAlert"></div>
                        <form id="blogEkleForm" enctype="multipart/form-data" method="post">

                            <input type="hidden" name="blog_user_id" value="<?php echo $users['users_id']; ?>">
                            <input type="hidden" name="blog_user_picture" value="<?php echo $users['users_picture']; ?>">
                            <input type="hidden" name="blog_user_name" value="<?php echo $users['users_name']." ".$users['users_surname']; ?>">
                            <input type="hidden" name="blog_date" value="<?php echo date('d.m.Y H:i:s'); ?>">

                            <div class="ui-block-content">
                                <h6>Kategori</h6>
                                <fieldset class="form-group">
                                    <select class="selectpicker form-control" name="blog_category">
                                        <?php
                                        //Kategorilerde blog,duyuru,etkinlik,ve forum için category_which değeri belirledik ve ona göre çekiyoruz
                                        //Blog için category_which değeri 1 iken diğerleri için de 2 3 4 olabilir
                                        $veri = $db->prepare("SELECT * FROM categories WHERE category_status='1' AND category_which='1'");
                                        $veri->execute(array());
                                        $v = $veri->fetchAll(PDO::FETCH_ASSOC);
                                        foreach ($v as $kat) {
                                            ?>
                                            <option value="<?php echo $kat['category_id']; ?>"><?php echo $kat['category_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </fieldset>
                                <br>

                                <h6>İçerik Başlığı</h6>

                                <div class="form-group label-floating is-empty">
                                    <label class="control-label">İçerik başlığı konuyu en kısa ve en iyi şekilde ifade
                                        eden bir başlık olmalı.</label>
                                    <input class="form-control" type="text" name="blog_title" placeholder="">
                                    <span class="material-input"></span>
                                </div>
                                <br>

                                <h6>İçerik</h6>
                                <div class="form-group with-icon label-floating is-empty">
                                    <textarea class="ckeditor" name="blog_text" placeholder=""></textarea>
                                </div>
                                <br>

                                <h6>İçerik Keyword</h6>

                                <div class="form-group label-floating is-empty">
                                    <label class="control-label">İçerik başlığı konuyu en kısa ve en iyi şekilde ifade
                                        eden bir başlık olmalı.</label>
                                    <input class="form-control" type="text" name="blog_keyw" placeholder="">
                                    <span class="material-input"></span>
                                </div>
                                <br>

                                <h6>İçerik Resim</h6>
                                <input name="blog_picture" type="file" class="form-control" id="inputDefault">
                            </div>
                            <div class="add-options-message">
                                <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"
                                   type="file" data-original-title="ADD PHOTOS">
                                    <svg class="olymp-camera-icon" data-toggle="modal"
                                         data-target="#update-header-photo">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-camera-icon"></use>
                                    </svg>
                                </a>
                                <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"
                                   data-original-title="TAG YOUR FRIENDS">
                                    <svg class="olymp-computer-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-computer-icon"></use>
                                    </svg>
                                </a>

                                <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"
                                   data-original-title="ADD LOCATION">
                                    <svg class="olymp-small-pin-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-small-pin-icon"></use>
                                    </svg>
                                </a>

                                <a class="btn btn-primary btn-md-2" id="blogEkleBtn">Kaydet</a>
                                <button class="btn btn-md-2 btn-border-think btn-transparent c-grey">Preview</button>

                            </div>

                        </form>
                    </div>
                    <div class="tab-pane" id="notice" role="tabpanel" aria-expanded="true">
                        <form>
                            <div class="author-thumb">
                                <img src="img/author-page.jpg" alt="author">
                            </div>
                            <div class="form-group with-icon label-floating is-empty">
                                <label class="control-label">Share what you are thinking here...</label>
                                <textarea class="form-control" placeholder=""></textarea>
                            </div>
                            <div class="add-options-message">
                                <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"
                                   data-original-title="ADD PHOTOS">
                                    <svg class="olymp-camera-icon" data-toggle="modal"
                                         data-target="#update-header-photo">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-camera-icon"></use>
                                    </svg>
                                </a>
                                <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"
                                   data-original-title="TAG YOUR FRIENDS">
                                    <svg class="olymp-computer-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-computer-icon"></use>
                                    </svg>
                                </a>

                                <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"
                                   data-original-title="ADD LOCATION">
                                    <svg class="olymp-small-pin-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-small-pin-icon"></use>
                                    </svg>
                                </a>

                                <button class="btn btn-primary btn-md-2">Post Status</button>
                                <button class="btn btn-md-2 btn-border-think btn-transparent c-grey">Preview</button>

                            </div>

                        </form>
                    </div>

                    <div class="tab-pane" id="event" role="tabpanel" aria-expanded="true">
                        <form>
                            <div class="author-thumb">
                                <img src="img/author-page.jpg" alt="author">
                            </div>
                            <div class="form-group with-icon label-floating is-empty">
                                <label class="control-label">Share what you are thinking here...</label>
                                <textarea class="form-control" placeholder=""></textarea>
                            </div>
                            <div class="add-options-message">
                                <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"
                                   data-original-title="ADD PHOTOS">
                                    <svg class="olymp-camera-icon" data-toggle="modal"
                                         data-target="#update-header-photo">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-camera-icon"></use>
                                    </svg>
                                </a>
                                <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"
                                   data-original-title="TAG YOUR FRIENDS">
                                    <svg class="olymp-computer-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-computer-icon"></use>
                                    </svg>
                                </a>

                                <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"
                                   data-original-title="ADD LOCATION">
                                    <svg class="olymp-small-pin-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-small-pin-icon"></use>
                                    </svg>
                                </a>

                                <button class="btn btn-primary btn-md-2">Post Status</button>
                                <button class="btn btn-md-2 btn-border-think btn-transparent c-grey">Preview</button>

                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <!-- ... end News Feed Form  -->
        </div>
    </div>

</div>


<!-- ... end Friends -->


<!-- Window-popup-CHAT for responsive min-width: 768px -->

<?php include_once 'modules/chat-popup.php'; ?>

<!-- ... end Window-popup-CHAT for responsive min-width: 768px -->

<a class="back-to-top" href="#">
    <img src="svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>

<!-- JS Scripts -->
<script src="js/jquery-3.2.1.js"></script>
<script src="boss/root/main.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script src="js/perfect-scrollbar.js"></script>
<script src="js/jquery.matchHeight.js"></script>
<script src="js/svgxuse.js"></script>
<script src="js/imagesloaded.pkgd.js"></script>
<script src="js/Headroom.js"></script>
<script src="js/velocity.js"></script>
<script src="js/ScrollMagic.js"></script>
<script src="js/jquery.waypoints.js"></script>
<script src="js/jquery.countTo.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/material.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/smooth-scroll.js"></script>
<script src="js/selectize.js"></script>
<script src="js/swiper.jquery.js"></script>
<script src="js/moment.js"></script>
<script src="js/daterangepicker.js"></script>
<script src="js/simplecalendar.js"></script>
<script src="js/fullcalendar.js"></script>
<script src="js/isotope.pkgd.js"></script>
<script src="js/ajax-pagination.js"></script>
<script src="js/Chart.js"></script>
<script src="js/chartjs-plugin-deferred.js"></script>
<script src="js/circle-progress.js"></script>
<script src="js/loader.js"></script>
<script src="js/run-chart.js"></script>
<script src="js/jquery.magnific-popup.js"></script>
<script src="js/jquery.gifplayer.js"></script>
<script src="js/mediaelement-and-player.js"></script>
<script src="js/mediaelement-playlist-plugin.min.js"></script>

<script src="js/base-init.js"></script>

<script src="Bootstrap/dist/js/bootstrap.bundle.js"></script>

</body>
</html>