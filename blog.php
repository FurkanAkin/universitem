<?php
include_once 'inc/inc-head.php';

?>
<body>


<!-- Fixed Sidebar Left -->
<?php include_once 'modules/left-sidebar.php'; ?>
<!-- ... end Fixed Sidebar Left -->


<!-- Fixed Sidebar Right -->

<!-- ... end Fixed Sidebar Right-Responsive -->


<!-- Header-BP -->
<?php include_once 'modules/header-modules.php'; ?>
<!-- ... end Responsive Header-BP -->


<!-- Main Header BlogV1 -->

<div class="header-spacer"></div>
<div class="row">
    <br>
</div>
<!-- ... end Main BlogV1 -->

<!-- Blog Arama -->
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="ui-block responsive-flex1200">
                <div class="ui-block-title">
                    <ul class="filter-icons">
                        <li>
                            <a href="#">
                                <img src="img/icon-chat2.png" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/icon-chat15.png" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/icon-chat9.png" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/icon-chat4.png" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/icon-chat6.png" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/icon-chat26.png" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="img/icon-chat27.png" alt="icon">
                            </a>
                        </li>
                    </ul>
                    <div class="w-select">
                        <div class="title">Filter By:</div>
                        <fieldset class="form-group">
                            <select class="selectpicker form-control">
                                <option value="NU">All Categories</option>
                                <option value="NU">Favourite</option>
                                <option value="NU">Likes</option>
                            </select>
                        </fieldset>
                    </div>

                    <div class="w-select">
                        <fieldset class="form-group">
                            <select class="selectpicker form-control">
                                <option value="NU">Date (Descending)</option>
                                <option value="NU">Date (Ascending)</option>
                            </select>
                        </fieldset>
                    </div>

                    <a href="#" data-toggle="modal" data-target="#create-photo-album" class="btn btn-primary btn-md-2">Filter</a>

                    <form class="w-search">
                        <div class="form-group with-button">
                            <input class="form-control" type="text" placeholder="Search Blog Posts......">
                            <button>
                                <svg class="olymp-magnifying-glass-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-magnifying-glass-icon"></use>
                                </svg>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Blog arama son -->

<!-- Boglar -->
<div class="container">
    <div class="row">

        <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-xs-12">

            <?php

            $Sayfa = @intval($_GET['sayfa']);
            if (!$Sayfa) $Sayfa = 1;
            $Say = $db->query("select * from blog order by blog_id DESC");
            $ToplamVeri = $Say->rowCount();
            $Limit = 10;
            $Sayfa_Sayisi = ceil($ToplamVeri / $Limit);
            if ($Sayfa > $Sayfa_Sayisi) {
                $Sayfa = 1;
            }
            $Goster = $Sayfa * $Limit - $Limit;
            $GorunenSayfa = 5;


            $blog = $db->query("SELECT * FROM blog INNER JOIN categories ON categories.category_id=blog.blog_category ORDER BY blog_id DESC LIMIT $Goster,$Limit");

            $blogAl = $blog->fetchAll(PDO::FETCH_ASSOC);
            ?>

            <?php foreach ($blogAl as $blogBas) { ?>
                <div class="ui-block">


                    <!-- Post -->

                    <article class="hentry blog-post blog-post-v3">

                        <div class="post-thumb blog-post">
                            <img src="<?php echo $blogBas['blog_picture']; ?>" alt="photo">
                            <a href="#" class="post-category bg-blue-light">
                                <?php echo $blogBas['category_name'];
                                ?>

                            </a>
                        </div>

                        <div class="post-content">

                            <div class="author-date">
                                <div class="author-thumb">
                                    <img alt="author" src="<?php echo $blogBas['blog_user_picture']; ?>" class="avatar">
                                </div>
                                <a class="h6 post__author-name fn"
                                   href="./users-profile.php?users_id=<?php echo $blogBas['blog_user_id']; ?>"><?php echo $blogBas['blog_user_name']; ?></a>
                                <div class="post__date">
                                    <time class="published" datetime="2017-04-24T18:18">
                                        - 12 hours ago
                                    </time>
                                    - önce yazdı.
                                </div>
                            </div>

                            <a href="blog-page.php?blog_id=<?php echo $blogBas['blog_id']; ?>"
                               class="h5 post-title"><?php echo strip_tags($blogBas['blog_title']); ?> </a>
                            <p><?php echo strip_tags(substr($blogBas['blog_text'], 0, 200)); ?> ...
                            </p>

                            <div class="post-additional-info inline-items">

                                <ul class="friends-harmonic">
                                    <li>
                                        <a href="#">
                                            <img src="img/icon-chat27.png" alt="icon">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/icon-chat2.png" alt="icon">
                                        </a>
                                    </li>
                                </ul>
                                <div class="names-people-likes">
                                    26
                                </div>

                                <div class="comments-shared">
                                    <a href="#" class="post-add-icon inline-items">
                                        <svg class="olymp-speech-balloon-icon">
                                            <use xlink:href="svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use>
                                        </svg>
                                        <span>0</span>
                                    </a>
                                </div>

                            </div>
                        </div>

                    </article>

                    <!-- ... end Post -->

                </div>

            <?php } ?>

            <!-- Pagination -->

            <nav aria-label="Page navigation">
                <ul class="pagination justify-content-center">
                    <?php if ($Sayfa > 1) { ?>
                        <li class="page-item">
                            <a class="page-link" href="blog.php?sayfa=1" tabindex="-1">İlk</a>
                        </li>
                    <?php } else { ?>

                        <li class="page-item disabled">
                            <a class="page-link" href="blog.php?sayfa=1" tabindex="-1">İlk</a>
                        </li>
                    <?php } ?>
                    <?php if ($Sayfa > 1) { ?>
                        <li class="page-item">
                            <a class="page-link" href="blog.php?sayfa=<?php echo $Sayfa - 1 ?>" tabindex="-1">Önceki</a>
                        </li>
                    <?php } else { ?>
                        <li class="page-item disabled">
                            <a class="page-link" href="blog.php?sayfa=<?php echo $Sayfa - 1 ?>" tabindex="-1">Önceki</a>
                        </li>
                    <?php } ?>
                    <?php

                    for ($i = $Sayfa - $GorunenSayfa; $i < $Sayfa + $GorunenSayfa + 1; $i++) {
                        if ($i > 0 and $i <= $Sayfa_Sayisi) {
                            if ($i == $Sayfa) {
                                echo '<li class="page-item disabled"><p class="page-link" href="#">' . $i . '</p></li>';
                            } else {
                                echo '<li class="page-item"><a class="page-link" href="blog.php?sayfa=' . $i . '">' . $i . '</a></li>';
                            }
                        }
                    }

                    ?>

                    <?php if ($Sayfa != $Sayfa_Sayisi) { ?>
                        <li class="page-item">
                            <a class="page-link" href="blog.php?sayfa=<?php echo $Sayfa + 1 ?>">Sonraki</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="blog.php?sayfa=<?php echo $Sayfa_Sayisi ?>">Son</a>
                        </li>
                    <?php } ?>
                </ul>
            </nav>

            <!-- ... end Pagination -->

        </div>

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12">
            <aside>

                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">Featured Posts</h6>
                    </div>
                </div>

                <div class="ui-block">


                    <!-- Post -->

                    <article class="hentry blog-post blog-post-v3 featured-post-item">

                        <div class="post-thumb">
                            <img src="img/post13.jpg" alt="photo">
                            <a href="#" class="post-category bg-purple">INSPIRATION</a>
                        </div>

                        <div class="post-content">

                            <div class="author-date">
                                by
                                <a class="h6 post__author-name fn" href="#">JACK SCORPIO</a>
                                <div class="post__date">
                                    <time class="published" datetime="2017-03-24T18:18">
                                        - 5 MONTHS ago
                                    </time>
                                </div>
                            </div>

                            <a href="#" class="h4 post-title">We went lookhunting in all the California bay area</a>

                            <div class="post-additional-info inline-items">

                                <ul class="friends-harmonic">
                                    <li>
                                        <a href="#">
                                            <img src="img/icon-chat26.png" alt="icon">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/icon-chat27.png" alt="icon">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/icon-chat2.png" alt="icon">
                                        </a>
                                    </li>
                                </ul>
                                <div class="names-people-likes">
                                    206
                                </div>

                                <div class="comments-shared">
                                    <a href="#" class="post-add-icon inline-items">
                                        <svg class="olymp-speech-balloon-icon">
                                            <use xlink:href="svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use>
                                        </svg>
                                        <span>97</span>
                                    </a>
                                </div>

                            </div>
                        </div>

                    </article>

                    <!-- ... end Post -->

                </div>

                <div class="ui-block">


                    <!-- Post -->

                    <article class="hentry blog-post blog-post-v3 featured-post-item">

                        <div class="post-thumb">
                            <img src="img/post14.jpg" alt="photo">
                            <a href="#" class="post-category bg-blue-light">THE COMMUNITY</a>
                        </div>

                        <div class="post-content">

                            <div class="author-date">
                                by
                                <a class="h6 post__author-name fn" href="#">JACK SCORPIO</a>
                                <div class="post__date">
                                    <time class="published" datetime="2017-03-24T18:18">
                                        - 2 MONTHS ago
                                    </time>
                                </div>
                            </div>

                            <a href="#" class="h4 post-title">We went lookhunting in all the California bay area</a>

                            <div class="post-additional-info inline-items">

                                <ul class="friends-harmonic">
                                    <li>
                                        <a href="#">
                                            <img src="img/icon-chat6.png" alt="icon">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/icon-chat7.png" alt="icon">
                                        </a>
                                    </li>
                                </ul>
                                <div class="names-people-likes">
                                    37
                                </div>

                                <div class="comments-shared">
                                    <a href="#" class="post-add-icon inline-items">
                                        <svg class="olymp-speech-balloon-icon">
                                            <use xlink:href="svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use>
                                        </svg>
                                        <span>62</span>
                                    </a>
                                </div>

                            </div>
                        </div>

                    </article>

                    <!-- ... end Post -->

                </div>

                <div class="ui-block">


                    <!-- Post -->

                    <article class="hentry blog-post blog-post-v3 featured-post-item">

                        <div class="post-thumb">
                            <img src="img/post15.jpg" alt="photo">
                            <a href="#" class="post-category bg-purple">INSPIRATION</a>
                        </div>

                        <div class="post-content">

                            <div class="author-date">
                                by
                                <a class="h6 post__author-name fn" href="#">MADDY SIMMONS </a>
                                <div class="post__date">
                                    <time class="published" datetime="2017-03-24T18:18">
                                        - 3 MONTHS ago
                                    </time>
                                </div>
                            </div>

                            <a href="#" class="h4 post-title">Check out this 10 yummy breakfast recipes</a>

                            <div class="post-additional-info inline-items">

                                <ul class="friends-harmonic">
                                    <li>
                                        <a href="#">
                                            <img src="img/icon-chat20.png" alt="icon">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/icon-chat11.png" alt="icon">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/icon-chat9.png" alt="icon">
                                        </a>
                                    </li>
                                </ul>
                                <div class="names-people-likes">
                                    88
                                </div>

                                <div class="comments-shared">
                                    <a href="#" class="post-add-icon inline-items">
                                        <svg class="olymp-speech-balloon-icon">
                                            <use xlink:href="svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use>
                                        </svg>
                                        <span>39</span>
                                    </a>
                                </div>

                            </div>
                        </div>

                    </article>

                    <!-- ... end Post -->
                </div>

                <div class="ui-block">


                    <!-- Post -->

                    <article class="hentry blog-post blog-post-v3 featured-post-item">

                        <div class="post-thumb">
                            <img src="img/post16.jpg" alt="photo">
                            <a href="#" class="post-category bg-primary">OLYMPUS NEWS</a>
                        </div>

                        <div class="post-content">

                            <div class="author-date">
                                by
                                <a class="h6 post__author-name fn" href="#">JACK SCORPIO</a>
                                <div class="post__date">
                                    <time class="published" datetime="2017-03-24T18:18">
                                        - 6 MONTHS ago
                                    </time>
                                </div>
                            </div>

                            <a href="#" class="h4 post-title">We optimized the Olympus App for better navigation</a>

                            <div class="post-additional-info inline-items">

                                <ul class="friends-harmonic">
                                    <li>
                                        <a href="#">
                                            <img src="img/icon-chat1.png" alt="icon">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/icon-chat26.png" alt="icon">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="img/icon-chat27.png" alt="icon">
                                        </a>
                                    </li>
                                </ul>
                                <div class="names-people-likes">
                                    93
                                </div>

                                <div class="comments-shared">
                                    <a href="#" class="post-add-icon inline-items">
                                        <svg class="olymp-speech-balloon-icon">
                                            <use xlink:href="svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use>
                                        </svg>
                                        <span>105</span>
                                    </a>
                                </div>

                            </div>
                        </div>

                    </article>

                    <!-- ... end Post -->

                </div>

            </aside>
        </div>

    </div>

</div>

<!-- Bloglar son -->


<!-- Window-popup-CHAT for responsive min-width: 768px -->

<?php include_once 'modules/chat-popup.php'; ?>

<!-- ... end Window-popup-CHAT for responsive min-width: 768px -->

<a class="back-to-top" href="#">
    <img src="svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>

<!-- JS Scripts -->
<script src="js/jquery-3.2.1.js"></script>
<script src="boss/root/main.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script src="js/perfect-scrollbar.js"></script>
<script src="js/jquery.matchHeight.js"></script>
<script src="js/svgxuse.js"></script>
<script src="js/imagesloaded.pkgd.js"></script>
<script src="js/Headroom.js"></script>
<script src="js/velocity.js"></script>
<script src="js/ScrollMagic.js"></script>
<script src="js/jquery.waypoints.js"></script>
<script src="js/jquery.countTo.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/material.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/smooth-scroll.js"></script>
<script src="js/selectize.js"></script>
<script src="js/swiper.jquery.js"></script>
<script src="js/moment.js"></script>
<script src="js/daterangepicker.js"></script>
<script src="js/simplecalendar.js"></script>
<script src="js/fullcalendar.js"></script>
<script src="js/isotope.pkgd.js"></script>
<script src="js/ajax-pagination.js"></script>
<script src="js/Chart.js"></script>
<script src="js/chartjs-plugin-deferred.js"></script>
<script src="js/circle-progress.js"></script>
<script src="js/loader.js"></script>
<script src="js/run-chart.js"></script>
<script src="js/jquery.magnific-popup.js"></script>
<script src="js/jquery.gifplayer.js"></script>
<script src="js/mediaelement-and-player.js"></script>
<script src="js/mediaelement-playlist-plugin.min.js"></script>

<script src="js/base-init.js"></script>

<script src="Bootstrap/dist/js/bootstrap.bundle.js"></script>

</body>
</html>